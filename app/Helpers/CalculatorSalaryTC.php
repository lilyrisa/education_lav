<?php

namespace App\Helpers;



class CalculatorSalaryTC
{
    private $trinh_do; // trình độ
    private $chuc_vu; // chức vụ
    private $chuc_danh; // chức danh
    private $kiem_nhiem; // kiêm nhiệm
    private $ngay_ki_hop_dong; //ngày kí hợp đồng
    private $xep_loai; // xếp loại [A, B, C, D]
    private $ngay_cong; // ngày công 
    private $quy_mo_sv; // quy mô sinh viên trực thuộc
    private $kmp_thang; // Kqm định mức bình quân tháng
    private $so_tiet_quy_doi; // số tiết quy đổi thực tế
    private $an_ca; // ăn ca (đơn giá tiền ăn)
    private $tien_dien_thoai; // tiền điện thoại
    private $muc_tham_gia_bao_hiem; // mức tham gia bảo hiểm
    private $BHXH;
    private $BHYT;
    private $BHTN;
    private $don_gia_dich_vu;
    private $he_so_hd_KLGD; //Hệ số Hd theo KLGD
    private $he_so_quy_mo_sv;// Hệ số Hd theo quy mô SV
    private $he_so_pc_kiem_nhiem; // Hệ số PC kiêm nhiệm
    private $he_so; // Hệ số PC kiêm nhiệm
    private $phu_cap_uu_dai; // Hệ số PC kiêm nhiệm


    public function __construct($arr){
        $this->trinh_do = strtolower($arr['trinh_do']);
        $this->chuc_vu = strtolower($arr['chuc_vu']);
        $this->chuc_danh = strtolower($arr['chuc_danh']);
        $this->kiem_nhiem = strtolower($arr['kiem_nhiem']);
        $this->ngay_ki_hop_dong = $arr['ngay_ki_hop_dong'];
        $this->xep_loai = strtoupper($arr['xep_loai']);
        $this->ngay_cong = $arr['ngay_cong'] != null ? (int)$arr['ngay_cong'] : 0;
        $this->quy_mo_sv = $arr['quy_mo_sv'] != null ? (int)$arr['quy_mo_sv'] : 0;
        $this->kmp_thang = $arr['kmp_thang'] != null ? (int)$arr['kmp_thang'] : 0;
        $this->so_tiet_quy_doi = $arr['so_tiet_quy_doi'] != null ? (double)$arr['so_tiet_quy_doi'] : 0;
        $this->an_ca = $arr['an_ca'] != null ? (int)$arr['an_ca'] : 0;
        $this->tien_dien_thoai = $arr['tien_dien_thoai'] != null ? (int)$arr['tien_dien_thoai'] : 0;
        $this->muc_tham_gia_bao_hiem = $arr['muc_tham_gia_bao_hiem'] != null ? (int) $arr['muc_tham_gia_bao_hiem'] : 0;
        $this->BHXH = $this->muc_tham_gia_bao_hiem*0.08;
        $this->BHYT = $this->muc_tham_gia_bao_hiem*0.015;
        $this->BHTN = $this->muc_tham_gia_bao_hiem*0.01;   
        $this->don_gia_dich_vu = $arr['don_gia_dich_vu'] != null ? (int)$arr['don_gia_dich_vu'] : 0;        
        $this->he_so_hd_KLGD = $arr['he_so_hd_KLGD'] != null ? (double)$arr['he_so_hd_KLGD'] : 0.0;        
        $this->he_so_quy_mo_sv = $arr['he_so_quy_mo_sv'] != null ? (double)$arr['he_so_quy_mo_sv'] : 0.0;        
        $this->he_so_pc_kiem_nhiem = $arr['he_so_pc_kiem_nhiem'] != null ? (double)$arr['he_so_pc_kiem_nhiem'] : 0.0;        
        $this->he_so = $arr['he_so'] != null ? (double)$arr['he_so'] : 0.0;        
        $this->phu_cap_uu_dai = $arr['phu_cap_uu_dai'] != null ? (int)$arr['phu_cap_uu_dai'] : 0;        
    }



    public function luong_tham_nien(){
        $result = $this->don_gia_dich_vu*$this->ngay_cong*$this->he_so;
        return $result;
    }

    public function tong_thu_nhap(){
        $result = $this->thanh_tien()+$this->phu_cap_dung_lop()+$this->luong_phu_cap_khac()+$this->luong_tham_nien()+($this->ngay_cong*$this->an_ca)+$this->tien_dien_thoai;
        return $result;
    }

    public function tong_thu_nhap_chiu_thue(){
        $result = $this->tong_thu_nhap()-($this->ngay_cong*$this->an_ca)-$this->tien_dien_thoai-$this->luong_tham_nien()-$this->phu_cap_uu_dai;
        return $result;
    }

    public function khau_tru_bao_hiem(){
        $result = $this->BHXH+$this->BHYT+$this->BHTN;
        return $result;
    }

    public function khau_tru_thue_tncn(){ // còn thiếu thu nhập phụ thuộc trong formula excel
       if($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 > 10000000){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 - 10000000)*0.15+5000000*0.05+5000000*0.1;
       }
       if(($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) <= 10000000 && ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) > 5000000){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 - 5000000) *0.1 + 5000000 *0.05;
       }
       if(($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) <= 5000000 && ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) > 0){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000)*0.05;
       }
       return 0;
    }
    public function thuc_nhan(){
        $result = $this->tong_thu_nhap() - $this->khau_tru_bao_hiem() - $this->khau_tru_thue_tncn(); // còn thiếu Số tiền bù trừ tạm ứng lương tháng
        return number_format($result, 0);
    }

    public function count_year_work(){  // double
        $startTimeStamp = strtotime($this->ngay_ki_hop_dong);
        $endTimeStamp = strtotime("now");
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberYear = $timeDiff/(86400*365);
        return number_format($numberYear, 2);
    }

    // public function khoi_luong_tiet_giang(){ // khối lương tiết giảng khoa/tháng
    //     return $this->
    // }

    public function don_gia_tiet_giang(){
        if($this->trinh_do == 'cn' && $this->chuc_danh == 'giảng viên'){
            return 60000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên'){
            return 70000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên 2'){
            return 80000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên 1'){
            return 100000;
        }
        if($this->trinh_do == 'ts' && $this->chuc_danh == 'giảng viên cao cấp'){
            return 150000;
        }
        return 165000;
    }

    public function don_gia_phu_cap_tiet_giang(){
        if($this->trinh_do == 'cn' && $this->chuc_danh == 'giảng viên'){
            return 45000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên'){
            return 65000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên 2'){
            return 72000;
        }
        if($this->trinh_do == 'ths' && $this->chuc_danh == 'giảng viên 1'){
            return 72000;
        }
        if($this->trinh_do == 'ts' && $this->chuc_danh == 'giảng viên cao cấp'){
            return 110000;
        }
        return 130000;
    }

    public function thanh_tien(){
        $result = $this->so_tiet_quy_doi*$this->don_gia_tiet_giang();
        return $result;
    }

    public function phu_cap_dung_lop(){
        if($this->xep_loai == 'A' ){
            return $this->kmp_thang*$this->don_gia_phu_cap_tiet_giang();
        }
        if($this->xep_loai == 'B'){
            return $this->kmp_thang*$this->don_gia_phu_cap_tiet_giang()*0.8;
        }
        if($this->xep_loai == 'C'){
            return $this->kmp_thang*$this->don_gia_phu_cap_tiet_giang()*0.6;
        }
        return $this->kmp_thang*$this->don_gia_phu_cap_tiet_giang()*0.4;
    }

    public function luong_phu_cap_khac(){
        return $this->don_gia_dich_vu*$this->ngay_cong*($this->he_so_hd_KLGD+$this->he_so_quy_mo_sv+$this->he_so_pc_kiem_nhiem);
    }

    public function toString($is = false, $options = null, $depth = null){
        if(!$is){
            return [
                'khoi_luong_tiet_giang_khoa' => number_format($this->so_tiet_quy_doi,1),
                'luong_theo_tiet_giang' => [
                    'don_gia_tiet_giang' => $this->don_gia_tiet_giang(),
                    'don_gia_phu_cap_tiet_giang' => $this->don_gia_phu_cap_tiet_giang(),
                    'thanh_tien' => $this->thanh_tien()
                ],
                'phu_cap_dung_lop' => $this->phu_cap_dung_lop(),
                'luong_ql_phu_cap_khac' => $this->luong_phu_cap_khac(),
                'phu_cap_tham_nien' => $this->luong_tham_nien(),
                'tong_thu_nhap' => $this->tong_thu_nhap(),
                'tong_thu_nhap_chiu_thue' => $this->tong_thu_nhap_chiu_thue(),
                'khau_tru_thue_tncn' => $this->khau_tru_thue_tncn(),
                'bao_hiem'=>[
                    'BHXH' => $this->BHXH,
                    'BHYT' => $this->BHYT,
                    'BHTN' => $this->BHTN,
                ],
                'thuc_nhan' => $this->thuc_nhan(),

            ];
        }else{
            return json_encode([
                'khoi_luong_tiet_giang_khoa' => number_format($this->so_tiet_quy_doi,1),
                'luong_theo_tiet_giang' => [
                    'don_gia_tiet_giang' => $this->don_gia_tiet_giang(),
                    'don_gia_phu_cap_tiet_giang' => $this->don_gia_phu_cap_tiet_giang(),
                    'thanh_tien' => $this->thanh_tien()
                ],
                'phu_cap_dung_lop' => $this->phu_cap_dung_lop(),
                'luong_ql_phu_cap_khac' => $this->luong_phu_cap_khac(),
                'phu_cap_tham_nien' => $this->luong_tham_nien(),
                'tong_thu_nhap' => $this->tong_thu_nhap(),
                'tong_thu_nhap_chiu_thue' => $this->tong_thu_nhap_chiu_thue(),
                'khau_tru_thue_tncn' => $this->khau_tru_thue_tncn(),
                'bao_hiem'=>[
                    'BHXH' => $this->BHXH,
                    'BHYT' => $this->BHYT,
                    'BHTN' => $this->BHTN,
                ],
                'thuc_nhan' => $this->thuc_nhan(),

            ],$options, $depth);
        }
    }
}
