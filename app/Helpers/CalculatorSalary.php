<?php

namespace App\Helpers;



class CalculatorSalary
{
    private $chuc_vu; // chức vụ
    private $chuc_danh; // chức danh
    private $kiem_nhiem; // kiêm nhiệm
    private $ngay_ki_hop_dong; //ngày kí hợp đồng
    private $xep_loai; // xếp loại [A, B, C, D]
    private $ngay_cong; // ngày công 
    private $nghi_le; // nghỉ lễ
    private $he_so_hq; // hệ số hq
    private $he_so_pc_kiem; // hệ số PC kiêm
    private $he_so_ht; // hệ số ht
    private $cong_ngoai_gio; // công ngoài giờ
    private $luong_ngoai_gio; // lương ngoài giờ
    private $phu_cap_dung_lop; // phụ cấp đứng lớp
    private $an_ca; // ăn ca (đơn giá tiền ăn)
    private $tien_dien_thoai; // tiền điện thoại
    private $cong_le_phep; // công lễ+phép
    private $PCUD; // PCƯĐ
    private $muc_tham_gia_bao_hiem; // mức tham gia bảo hiểm
    private $BHXH;
    private $BHYT;
    private $BHTN;
    private $don_gia_dich_vu;

    public function __construct($arr){
        $this->chuc_vu = $arr['chuc_vu'];
        $this->chuc_danh = $arr['chuc_danh'];
        $this->kiem_nhiem = $arr['kiem_nhiem'];
        $this->ngay_ki_hop_dong = $arr['ngay_ki_hop_dong'];
        $this->xep_loai = $arr['xep_loai'];
        $this->ngay_cong = $arr['ngay_cong'] != null ? (int)$arr['ngay_cong'] : 0;
        $this->nghi_le = $arr['nghi_le'] != null ? (int)$arr['nghi_le'] : 0;
        $this->he_so_hq = $arr['he_so_hq'] != null ? (double)$arr['he_so_hq'] : 0.0;
        $this->he_so_pc_kiem = $arr['he_so_pc_kiem'] != null ? (double)$arr['he_so_pc_kiem'] : 0.0;
        $this->he_so_ht = $arr['he_so_ht'] != null ? (double)$arr['he_so_ht'] : 0.0;
        $this->cong_ngoai_gio = $arr['cong_ngoai_gio'] != null ? (int)$arr['cong_ngoai_gio'] : 0;
        $this->luong_ngoai_gio = $arr['luong_ngoai_gio'] != null ? (int)$arr['luong_ngoai_gio'] : 0;
        $this->phu_cap_dung_lop = $arr['phu_cap_dung_lop'] != null ? (int)$arr['phu_cap_dung_lop'] : 0;
        $this->an_ca = $arr['an_ca'] != null ? (int)$arr['an_ca'] : 0;
        $this->tien_dien_thoai = $arr['tien_dien_thoai'] != null ? (int)$arr['tien_dien_thoai'] : 0;
        $this->cong_le_phep = $arr['cong_le_phep'] != null ? (int)$arr['cong_le_phep'] : 0;
        $this->PCUD = $arr['PCUD'] != null ? (int)$arr['PCUD'] : 0;
        $this->muc_tham_gia_bao_hiem = $arr['muc_tham_gia_bao_hiem'] != null ? (int) $arr['muc_tham_gia_bao_hiem'] : 0;
        $this->BHXH = $this->muc_tham_gia_bao_hiem*0.08;
        $this->BHYT = $this->muc_tham_gia_bao_hiem*0.015;
        $this->BHTN = $this->muc_tham_gia_bao_hiem*0.01;   
        $this->don_gia_dich_vu = $arr['don_gia_dich_vu'] != null ? (int)$arr['don_gia_dich_vu'] : 0;        
    }

    public function luong_cong_viec(){

        if($this->xep_loai == 'A' || $this->xep_loai == 'a'){
            $result = $this->ngay_cong*(1+$this->he_so_hq+$this->he_so_pc_kiem)*$this->don_gia_dich_vu + $this->nghi_le*(1+$this->he_so_hq)*$this->don_gia_dich_vu;
            return $result;
        }

        if($this->xep_loai == 'B' || $this->xep_loai == 'b'){
            $result = $this->ngay_cong*(1*0.9+$this->he_so_hq+$this->he_so_pc_kiem)*$this->don_gia_dich_vu+$this->nghi_le*(1+$this->he_so_hq)*$this->don_gia_dich_vu;
            return $result;
        }

        if($this->xep_loai == 'C' || $this->xep_loai == 'c'){
            $result = $this->ngay_cong*(1*0.8+$this->he_so_hq+$this->he_so_pc_kiem)*$this->don_gia_dich_vu + $this->nghi_le*(1+$this->he_so_hq)*$this->don_gia_dich_vu;
            return $result;
        }

        if($this->xep_loai == 'D' || $this->xep_loai == 'd'){
            $result = $this->ngay_cong*(1*0.7+$this->he_so_hq+$this->he_so_pc_kiem)*$this->don_gia_dich_vu + $this->nghi_le*(1+$this->he_so_hq)*$this->don_gia_dich_vu;
            return $result;
        }
    }

    public function he_so_ht(){
        if($this->he_so_ht == null){
            return 0.165;
        }
        return $this->he_so_ht;
    }

    public function luong_tham_nien(){
        $result = $this->don_gia_dich_vu*$this->ngay_cong*$this->he_so_ht();
        return $result;
    }

    public function luong_le_phep(){
        $result = $this->cong_le_phep*200000*(1+$this->he_so_hq);
        return $result;
    }

    public function tong_thu_nhap(){
        $result = $this->luong_cong_viec()+$this->luong_tham_nien()+$this->luong_ngoai_gio+$this->phu_cap_dung_lop+(($this->ngay_cong+$this->cong_ngoai_gio)*$this->an_ca)+$this->tien_dien_thoai+$this->luong_le_phep();
        return $result;
    }

    public function tong_thu_nhap_chiu_thue(){
        $result = $this->tong_thu_nhap()-(($this->ngay_cong+$this->cong_ngoai_gio)*$this->an_ca)-$this->tien_dien_thoai-$this->PCUD;
        return $result;
    }

    public function khau_tru_bao_hiem(){
        $result = $this->BHXH+$this->BHYT+$this->BHTN;
        return $result;
    }

    public function khau_tru_thue_tncn(){ // còn thiếu  *AO8*3600000 trong formula excel
       if($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 > 10000000){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 - 10000000)*0.15+5000000*0.05+5000000*0.1;
       }
       if(($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) <= 10000000 && ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) > 5000000){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000 - 5000000) *0.1 + 5000000 *0.05;
       }
       if(($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) <= 5000000 && ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000) > 0){
           return ($this->tong_thu_nhap_chiu_thue() - $this->khau_tru_bao_hiem() - 11000000)*0.05;
       }
       return 0;
    }
    public function thuc_nhan(){
        $result = $this->tong_thu_nhap() - $this->khau_tru_bao_hiem() - $this->khau_tru_thue_tncn(); // còn thiếu Số tiền bù trừ tạm ứng lương tháng
        return number_format($result, 0);
    }

    public function count_year_work(){  // double
        $startTimeStamp = strtotime($this->ngay_ki_hop_dong);
        $endTimeStamp = strtotime("now");
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberYear = $timeDiff/(86400*365);
        return number_format($numberYear, 2);
    }

    public function toString($is = false, $options = null, $depth = null){
        if(!$is){
            return [
                'thanh_tien' => $this->luong_cong_viec(),
                'he_so_ht' => $this->he_so_ht(),
                'luong_tham_nien' => $this->luong_tham_nien(),
                'luong_le_phep' => $this->luong_le_phep(),
                'tong_thu_nhap' => $this->tong_thu_nhap(),
                'tong_thu_nhap_chiu_thue' => $this->tong_thu_nhap_chiu_thue(),
                'khau_tru_bao_hiem' => $this->khau_tru_bao_hiem(),
                'khau_tru_thue_tncn' => $this->khau_tru_thue_tncn(),
                'thuc_nhan' => $this->thuc_nhan(),
                'bao_hiem'=>[
                    'BHXH' => $this->BHXH,
                    'BHYT' => $this->BHYT,
                    'BHTN' => $this->BHTN,
                ],
            ];
        }else{
            return json_encode([
                'thanh_tien' => $this->luong_cong_viec(),
                'he_so_ht' => $this->he_so_ht(),
                'luong_tham_nien' => $this->luong_tham_nien(),
                'luong_le_phep' => $this->luong_le_phep(),
                'tong_thu_nhap' => $this->tong_thu_nhap(),
                'tong_thu_nhap_chiu_thue' => $this->tong_thu_nhap_chiu_thue(),
                'khau_tru_bao_hiem' => $this->khau_tru_bao_hiem(),
                'khau_tru_thue_tncn' => $this->khau_tru_thue_tncn(),
                'thuc_nhan' => $this->thuc_nhan(),
                'bao_hiem'=>[
                    'BHXH' => $this->BHXH,
                    'BHYT' => $this->BHYT,
                    'BHTN' => $this->BHTN,
                ]
            ],$options, $depth);
        }
    }
}
