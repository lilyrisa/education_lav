<?php

namespace App;

use App\Models\BloodGroup;
use App\Models\Lga;
use App\Models\Nationality;
use App\Models\StaffRecord;
use App\Models\State;
use App\Models\UserType;
use App\Models\StudentRecord;
use App\Models\FaceRecogn;
use App\Models\SalaryStore;
use App\Models\SalaryStoreAccontant;
use App\Models\SalaryParameter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'phone', 'phone2', 'dob', 'gender', 'photo', 'address', 'bg_id', 'password', 'nal_id', 'state_id', 'lga_id', 'code', 'user_type', 'email_verified_at','level_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function student_record()
    {
        return $this->hasOne(StudentRecord::class);
    }

    public function lga()
    {
        return $this->belongsTo(Lga::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class, 'nal_id');
    }

    public function blood_group()
    {
        return $this->belongsTo(BloodGroup::class, 'bg_id');
    }

    public function staff()
    {
        return $this->hasMany(StaffRecord::class);
    }

    public function user_type(){

        return $this->belongsTo(UserType::class);
    }

    public function FaceRecognModel()
    {
        return $this->hasOne(FaceRecogn::class, 'user_face_id');
    }

    public function SalaryStore()
    {
        return $this->hasOne(SalaryStore::class, 'user_id_salary');
    }
    public function SalaryStoreAccontant()
    {
        return $this->hasOne(SalaryStoreAccontant::class, 'user_id_salary');
    }
    public function SalaryParameter()
    {
        return $this->hasOne(SalaryParameter::class, 'user_id_para');
    }
}
