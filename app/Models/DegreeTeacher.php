<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DegreeTeacher extends Model
{
    use HasFactory;

    protected $fillable = ['user_type_id', 'dergee_name'];

    public function user_type(){

        return $this->belongsTo(UserType::class);
    }
}
