<?php

namespace App\Models;

use Eloquent;
use App\User;
use App\DegreeTeacher;

class UserType extends Eloquent
{
    protected $fillable = ['title', 'name', 'level'];

    public function user(){

        return $this->hasMany(User::class);
    }
    public function DegreeTeacher(){

        return $this->hasMany(DegreeTeacher::class);
    }
}
