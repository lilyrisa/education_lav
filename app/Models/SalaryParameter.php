<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\User;

class SalaryParameter extends Model
{
    use HasFactory;
    
    protected $fillable = ['user_id_para', 'xep_loai', 'ngay_cong', 'don_gia_dich_vu', 'an_ca', 'tien_dien_thoai', 'he_so_pc_kiem_nhiem', 'phu_cap_uu_dai', 'muc_tham_gia_bao_hiem'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id_para');
    }
}
