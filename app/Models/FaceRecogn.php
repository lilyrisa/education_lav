<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\User;
use Illuminate\Database\Eloquent\Model;

class FaceRecogn extends Model
{
    use HasFactory;


    public function User()
    {
        return $this->belongsTo(User::class, 'user_face_id');
    }
}
