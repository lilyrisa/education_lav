<?php

namespace App\Repositories;

use App\User;
use App\Models\UserType;


class UserTypeRepo {


    public function update($id, $data)
    {
        return UserType::find($id)->update($data);
    }

    public function delete($id)
    {
        return UserType::destroy($id);
    }

    public function create($data)
    {
        return UserType::create($data);
    }

    public function getUserByType($user)
    {
        return UserType::with('user')->orderBy('name', 'asc')->get();
    }

    public function getListUserByType($user_type){
        $match = [];
        foreach($user_type as $ut){
            if($ut->level == 3 || $ut->level == 5){
                $match[] = $ut->title;
            }
        }
        return User::whereIn('user_type', $match)->orderBy('name', 'asc')->get();
    }

    public function find($id)
    {
        return UserType::find($id);
    }

    public function getAll()
    {
        return UserType::orderBy('name', 'asc')->get();
    }

    public function getListType($type)
    {
        if($type == 'staff'){
            return UserType::where('level', '5')->get();
        }
        if($type == 'teacher'){
            return UserType::where('level', '3')->get();
        }

        
    }

}