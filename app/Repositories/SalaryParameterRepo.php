<?php

namespace App\Repositories;

use App\User;
use App\Models\SalaryParameter;


class SalaryParameterRepo {


    public function update($id, $data)
    {
        unset($data['user_id_para']);
        return SalaryParameter::find($id)->update($data);
    }

    public function UpdateViaUserId($data){
        $id = (SalaryParameter::where('user_id_para',$data['user_id_para'])->get()->first())->id;
        return $this->update($id, $data);
    }

    public function delete($id)
    {
        return SalaryParameter::destroy($id);
    }

    public function create($data)
    {
        if(SalaryParameter::where('user_id_para',$data['user_id_para'])->count() == 0){
            return SalaryParameter::create($data);
        }
        return $this->UpdateViaUserId($data);
    }

    public function getUser()
    {
        return SalaryParameter::with('user')->orderBy('user_id_para', 'asc')->get();
    }


    public function find($id)
    {
        return SalaryParameter::find($id);
    }

    public function findWhere($id)
    {
        return SalaryParameter::with('user')->where('id',$id)->get()->first();
    }

    public function getAll()
    {
        return SalaryParameter::orderBy('user_id_para', 'asc')->get();
    }
    public function getUserById($id)
    {
        return SalaryParameter::with('user')->where('user_id_para', $id)->orderBy('user_id_para', 'asc')->get();
    }

}