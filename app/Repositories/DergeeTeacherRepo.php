<?php

namespace App\Repositories;

use App\User;
use App\Models\UserType;
use App\Models\DegreeTeacher;


class DergeeTeacherRepo {


    public function update($id, $data)
    {
        return DegreeTeacher::find($id)->update($data);
    }

    public function delete($id)
    {
        return DegreeTeacher::destroy($id);
    }

    public function create($data)
    {
        return DegreeTeacher::create($data);
    }

    public function getUserTypeById($id)
    {
        return DegreeTeacher::with('user_type')->orderBy('dergee_name', 'asc')->get();
    }

    public function getUserTypeBySelf($user_type)
    {
        $match = [];
        foreach($user_type as $ut){
            if($ut->level == 3){
                $match[] = $ut->id;
            }
        }
        return DegreeTeacher::whereIn('user_type_id', $match)->with('user_type')->orderBy('dergee_name', 'asc')->get();
    }

    public function find($id)
    {
        return DegreeTeacher::find($id);
    }

    public function getAll()
    {
        return DegreeTeacher::orderBy('dergee_name', 'asc')->get();
    }

    public function getListTypeId($type)
    {
        return DegreeTeacher::where('user_type_id', $type)->get();
      
    }

}