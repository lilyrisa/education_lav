<?php

namespace App\Http\Controllers\SupportTeam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Helpers\Qs;

use App\Repositories\UserTypeRepo as Utype;
use App\Repositories\DergeeTeacherRepo as DergeeRepo;
use App\Http\Requests\Dergee\DergeeTeacherCreate;
use App\Http\Requests\Dergee\DergeeTeacherUpdate;

class DergeeTeacherController extends Controller
{

    protected $user_type, $dergee;

    public function __construct(Utype $user_type, DergeeRepo $dergee)
    {
        $this->user_type = $user_type;
        $this->dergee = $dergee;

    }

    public function index(){
        $user_type = $this->user_type->getAll();
        $dergee = $this->dergee->getUserTypeBySelf($user_type);
        $user_type = $this->user_type->getListType('teacher');
        // dd($dergee);
        return view('pages.support_team.dergee_teacher.index', ['dergee' => $dergee, 'user_type' => $user_type]);
    }

    public function save(DergeeTeacherCreate $reg){
        $data = $reg->only(['user_type_id', 'dergee_name']);
        // dd($data);
        $this->dergee->create($data);
        return Qs::jsonStoreOk();
    }

    public function get_update($c_id){
        $dergee = $this->dergee->find($c_id);
        $user_type = $this->user_type->getListType('teacher');
        return view('pages.support_team.dergee_teacher.edit', ['dergee' => $dergee, 'user_type' => $user_type]);
    }

    public function update(DergeeTeacherUpdate $reg, $u_id){
        $data = $reg->only(['user_type_id', 'dergee_name']);
        // dd($u_id);
        $this->dergee->update($u_id, $data);
        return back()->with('flash_success', __('msg.update_ok'));
    }

    public function destroy($d_s_id)
    {
        $this->user_type->delete($d_s_id);
        return back()->with('flash_success', __('msg.del_ok'));
    }
    
}
