<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Helpers\FaceRecogn;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Models\FaceRecogn as FaceRecognModel;


class FaceController extends Controller
{
    public function login(){
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('auth.login_face');
    }

    public function loginFace(Request $request){
        $image = $request->input('avatar');
        $image = explode( ',', $image);
        $image = $image[1];
        $imageName = md5(Carbon::now()->timestamp) . '.png';
        $path = 'image/'.$imageName;
        Storage::disk('local')->put($path, base64_decode($image));
        $path = asset('storage').'/'.$path;
        $return = FaceRecogn::get_username_with_image($image);
        // dd($return);
        Storage::disk('local')->delete($path);
        $return = json_decode($return, true);
        if(!empty($return)){
            $return = $return[0];
            $username = $return['id'];
            $id_user = FaceRecognModel::where('id_cloud', $username)->first();
            if($id_user == null){
                return response()->json(['is' => false, 'messenge' => 'Không nhận diện được nhân viên nào']);
            }
            $user = User::find($id_user->user_face_id)->first();
            if($user != null){
                Auth::loginUsingId($user->id);
                return response()->json(['is' => true, 'messenge' => $user]);
            }

            return response()->json(['is' => false, 'messenge' => 'Không nhận diện được nhân viên nào']);
        }

        return response()->json(['is' => false, 'messenge' => 'Không nhận diện được nhân viên nào']);
    }

    public function index(){
        $detect = FaceRecognModel::where('user_face_id',Auth::id())->first();
        if($detect){
            $face = FaceRecogn::get_list_person();
            $user = User::find(Auth::id());
            $key = array_search($user->username, array_column($face, 'name'));
            $face = $face[$key];
            return view('pages.support_team.face_recogn.face_recogn',['user' => $user, 'face' => $face]);
        }       
        return redirect()->route('face.createper');
        
    }

    public function post(Request $request){
        $image = $request->input('avatar');
        $image = explode( ',', $image);
        $image = $image[1];
        $imageName = md5(Carbon::now()->timestamp) . '.png';
        $path = 'image/'.$imageName;
        Storage::disk('local')->put($path, base64_decode($image));
        $user = User::where('id', Auth::id())->with('FaceRecognModel')->first();
        $arr = ["photo" => asset('storage').'/'.$path, "id" => $user->FaceRecognModel->id_cloud];
        $face = FaceRecogn::add_face_to_person_username($arr);
        Storage::disk('local')->delete($path);
        return response()->json(['source' => $face, 'is' =>true]);
    }

    public function detect(Request $request){
        $image = $request->input('avatar');
        $image = explode( ',', $image);
        $image = $image[1];
        $imageName = md5(Carbon::now()->timestamp) . '.png';
        $path = 'image/'.$imageName;
        Storage::disk('local')->put($path, base64_decode($image));
        $path = asset('storage').'/'.$path;
        $return = FaceRecogn::get_username_with_image($image);
        Storage::disk('local')->delete($path);
        $return = json_decode($return, true);
        if(!empty($return)){
            $return = $return[0];
            $username = $return['id'];
            $user = FaceRecognModel::where('id_cloud', $username)->with('User')->first();
            if($user != null){
                return response()->json(['is' => true, 'messenge' => $user->user]);
            }
            return response()->json(['is' => false, 'messenge' => 'Không nhận diện được nhân viên nào 1']);
        }
        return response()->json(['is' => false, 'messenge' => 'Không nhận diện được nhân viên nào']);
    }

    public function create_person(){
        $user = User::find(Auth::id());
        $detect = FaceRecognModel::where('user_face_id',Auth::id())->first();
        if($detect){
            return redirect()->route('face.index');
            
        }   
        return view('pages.support_team.face_recogn.face_recogn_create', ['user' => $user]);
    }

    public function create_person_post(Request $request){
        $image = $request->input('avatar');
        $image = explode( ',', $image);
        $image = $image[1];
        $imageName = md5(Carbon::now()->timestamp) . '.png';
        $path = 'image/'.$imageName;
        Storage::disk('local')->put($path, base64_decode($image));
        $user = User::find(Auth::id());
        $face = new FaceRecogn($user);
        $response = $face->create_person_cloud_with_photo($path);

        if(isset($response['error'])){
            return response()->json(['is' =>false]);
        }
        $face_detected = new FaceRecognModel();
        $face_detected->user_face_id = Auth::id();
        $face_detected->id_cloud = $response['id'];
        $face_detected->save();
        return response()->json(['is' =>true]);
    }
}
