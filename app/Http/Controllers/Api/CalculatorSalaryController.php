<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\CalculatorSalary;
use App\Helpers\CalculatorSalaryTC;
use response;

class CalculatorSalaryController extends Controller
{
    public function luong_cong_viec(Request $request){
        $arr = $request->all();
        $calculator = new CalculatorSalary($arr);
        return response()->json($calculator->toString());

    }

    public function luong_cong_viec_tc(Request $request){
        $arr = $request->all();
        $calculator = new CalculatorSalaryTC($arr);
        return response()->json($calculator->toString());

    }
}
