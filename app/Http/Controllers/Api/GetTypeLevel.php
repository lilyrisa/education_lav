<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Level;
use App\Helpers\Qs;
use response;

class GetTypeLevel extends Controller
{
    public function get_level_user_type(Request $request){
        $lv = Level::where('user_type_level',$request->input('level'))->get()->toArray();
        $lv = array_map(function($item){
            $item['id'] = Qs::hash($item['id']);
            return $item;
        },$lv);
        return \response()->json($lv);
    }
}
