<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserType;
use App\Http\Requests\TypeUser\StaffUser;
use App\Http\Requests\TypeUser\StaffUserUpdate;
use App\Http\Requests\TypeUser\TeacherUser;
use App\Http\Requests\TypeUser\TeacherUserUpdate;
use App\Helpers\Qs;
use App\Repositories\UserRepo;
use App\Repositories\UserTypeRepo as Utype;


class TypeForUser extends Controller
{

    protected $user_type;

    public function __construct(Utype $user_type)
    {
        $this->user_type = $user_type;

    }

    public function staff(){
        $user_type = $this->user_type->getListType('staff');
        // dd($user_type);
        return view('pages.support_team.user_for_type.staff.index', ['user_type' => $user_type]);
    }

    public function save_staff(StaffUser $reg){
        $data = $reg->all();
        $u_type = new UserType();
        $u_type->title = $data['title'];
        $u_type->name = $data['name'];
        $u_type->level = $data['level'];
        $u_type->save();
        return Qs::jsonStoreOk();
    }

    public function get_update_staff($c_id){
        $user_type = $this->user_type->find($c_id);
        return view('pages.support_team.user_for_type.staff.edit', ['user_type' => $user_type]);
    }

    public function update_staff(StaffUserUpdate $reg, $u_id){
        $data = $reg->all();
        // dd($u_id);
        $this->user_type->update($u_id, $data);
        return Qs::jsonUpdateOk();
    }

    public function destroy_staff($d_s_id)
    {
        $this->user_type->delete($d_s_id);
        return back()->with('flash_success', __('msg.del_ok'));
    }

    public function teacher(){
        $user_type = $this->user_type->getListType('teacher');
        // dd($user_type);
        return view('pages.support_team.user_for_type.teacher.index', ['user_type' => $user_type]);
    }

    public function save_teacher(TeacherUser $reg){
        $data = $reg->all();
        $u_type = new UserType();
        $u_type->title = $data['title'];
        $u_type->name = $data['name'];
        $u_type->level = $data['level'];
        $u_type->save();
        return Qs::jsonStoreOk();
    }

    public function get_update_teacher($c_id){
        $user_type = $this->user_type->find($c_id);
        return view('pages.support_team.user_for_type.teacher.edit', ['user_type' => $user_type]);
    }

    public function update_teacher(TeacherUserUpdate $reg, $u_id){
        $data = $reg->all();
        // dd($u_id);
        $this->user_type->update($u_id, $data);
        return Qs::jsonUpdateOk();
    }

    public function destroy_teacher($d_t_id)
    {
        $this->user_type->delete($d_t_id);
        return back()->with('flash_success', __('msg.del_ok'));
    }
}
