<?php

namespace App\Http\Controllers\salary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserTypeRepo;
use App\Models\UserType;
use App\Models\Level;
use App\User;
use App\Models\SalaryStoreAccontant;
use App\Models\SalaryStore;
use response;
use App\Helpers\CalculatorSalary;
use App\Helpers\CalculatorSalaryTC;
use App\Jobs\SendEmail;
use App\Jobs\PhoneSMS;

class SalaryCalculatorController extends Controller
{
    protected $user_type;

    public function __construct(UserTypeRepo $user_type)
    {
        $this->user_type = $user_type;
    }


    public function index(){
        $user_type = UserType::where('level', 3)->get();
        $lsUser = $this->user_type->getListUserByType($user_type);
        foreach($lsUser as &$item){
            $item->emp = explode('/',$item->username);
            if(count($item->emp) == 1){
                $item->emp = null;
            }else{
                $item->emp = '01/'.$item->emp[3].'/'.$item->emp[2];
            }
        }
        // dd($lsUser);
        return view('pages.support_team.salary_calculate.index',['lsUser' => $lsUser]);
    }

    public function getPositionUser(Request $request){
        $id = $request->input('user_id');
        $user = User::with('SalaryParameter')->where('id',$id)->get()->first();
        $user_type = UserType::where('title', $user->user_type)->get()->toArray();

        $user->emp = explode('/',$user->username);
        if(count($user->emp) == 1){
            $user->emp = null;
        }else{
            $user->emp = $user->emp[2].'-'.$user->emp[3].'-01';
        }
        $user_type[] = $user->emp;
        $user_type[] = $user;
        return response()->json($user_type);
    }

    public function getLevelType(Request $request){
        $type = UserType::where('title', $request->input('user_type'))->get()->first();
        // dd($type);
        $type = $type->level;
        return response()->json(['level' => $type]);
    }

    public function Calculator(Request $request){
        $arr = $request->all();
        $calculator = new CalculatorSalaryTC($arr);
        return response()->json($calculator->toString());
    }

    public function SaveNoty(Request $request){
        $user_id = $request->input('account_id');
        $data = $request->input('data');
        $user = User::where('id', $user_id)->get()->first();
        if(SalaryStore::where('user_id_salary', $user_id)->whereYear('created_at', '=', now()->year)->whereMonth('created_at', '=', now()->month)->count() == 0){
            $salary = new SalaryStore();
            $salary->user_id_salary = $user_id;
            $salary->khoi_luong_tiet_giang_khoa = $data['khoi_luong_tiet_giang_khoa'];
            $salary->don_gia_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_tiet_giang'];
            $salary->don_gia_phu_cap_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_phu_cap_tiet_giang'];
            $salary->thanh_tien = $data['luong_theo_tiet_giang']['thanh_tien'];
            $salary->phu_cap_dung_lop = $data['phu_cap_dung_lop'];
            $salary->luong_ql_phu_cap_khac = $data['luong_ql_phu_cap_khac'];
            $salary->phu_cap_tham_nien = $data['phu_cap_tham_nien'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }else{
            $salary = SalaryStore::where('user_id_salary',$user_id)->first();
            $salary = SalaryStore::find($salary->id);
            $salary->khoi_luong_tiet_giang_khoa = $data['khoi_luong_tiet_giang_khoa'];
            $salary->don_gia_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_tiet_giang'];
            $salary->don_gia_phu_cap_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_phu_cap_tiet_giang'];
            $salary->thanh_tien = $data['luong_theo_tiet_giang']['thanh_tien'];
            $salary->phu_cap_dung_lop = $data['phu_cap_dung_lop'];
            $salary->luong_ql_phu_cap_khac = $data['luong_ql_phu_cap_khac'];
            $salary->phu_cap_tham_nien = $data['phu_cap_tham_nien'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }
        

        SendEmail::dispatch($user, $user->email, [
            'title' => '('.$user->username.')Lương của bạn tháng '.date('Y-m-d'),
            'body' => 'Lương thực nhận: '.$data["thuc_nhan"].' VND. Truy cập <a href="'.route('home_index').'">'.route('home_index').'</a> để xem chi tiết',
            'image' => 'https://raw.githubusercontent.com/lime7/responsive-html-template/master/index/intro__bg.png'
            ]);
        $phone = $user->phone;
        $phone = new PhoneSms($user->username.' - Muc luong thuc nhan: '.$data['thuc_nhan'].'VND. Duoc khoi tao luc '.date('Y-m-d').'. Truy cap tai day de xem day du hon '.route('home_index'), $phone);
        $phone->sendMessage();
        return response()->json(['status' => true]);
    }

    public function SavewithoutSaveNoty(Request $request){
        $user_id = $request->input('account_id');
        $data = $request->input('data');
        $user = User::where('id', $user_id)->get()->first();
        if(SalaryStore::where('user_id_salary', $user_id)->whereYear('created_at', '=', now()->year)->whereMonth('created_at', '=', now()->month)->count() == 0){
            $salary = new SalaryStore();
            $salary->user_id_salary = $user_id;
            $salary->khoi_luong_tiet_giang_khoa = $data['khoi_luong_tiet_giang_khoa'];
            $salary->don_gia_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_tiet_giang'];
            $salary->don_gia_phu_cap_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_phu_cap_tiet_giang'];
            $salary->thanh_tien = $data['luong_theo_tiet_giang']['thanh_tien'];
            $salary->phu_cap_dung_lop = $data['phu_cap_dung_lop'];
            $salary->luong_ql_phu_cap_khac = $data['luong_ql_phu_cap_khac'];
            $salary->phu_cap_tham_nien = $data['phu_cap_tham_nien'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }else{
            $salary = SalaryStore::where('user_id_salary',$user_id)->first();
            $salary = SalaryStore::find($salary->id);
            $salary->khoi_luong_tiet_giang_khoa = $data['khoi_luong_tiet_giang_khoa'];
            $salary->don_gia_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_tiet_giang'];
            $salary->don_gia_phu_cap_tiet_giang = $data['luong_theo_tiet_giang']['don_gia_phu_cap_tiet_giang'];
            $salary->thanh_tien = $data['luong_theo_tiet_giang']['thanh_tien'];
            $salary->phu_cap_dung_lop = $data['phu_cap_dung_lop'];
            $salary->luong_ql_phu_cap_khac = $data['luong_ql_phu_cap_khac'];
            $salary->phu_cap_tham_nien = $data['phu_cap_tham_nien'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }
        return response()->json(['status' => true]);
        
    }

    public function Accountantindex(){
        $user_type = UserType::where('level', 5)->get();
        $lsUser = $this->user_type->getListUserByType($user_type);
        foreach($lsUser as &$item){
            $item->emp = explode('/',$item->username);
            if(count($item->emp) == 1){
                $item->emp = null;
            }else{
                $item->emp = '01/'.$item->emp[3].'/'.$item->emp[2];
            }
        }
        // dd($lsUser);
        return view('pages.support_team.salary_calculate.accountant',['lsUser' => $lsUser]);
    }

    public function AccountantGetPositionUser(Request $request){
        $id = $request->input('user_id');
        $user = User::with('SalaryParameter')->where('id',$id)->get()->first();
        $user_type = UserType::where('title', $user->user_type)->get()->toArray();

        $user->emp = explode('/',$user->username);
        if(count($user->emp) == 1){
            $user->emp = null;
        }else{
            $user->emp = $user->emp[2].'-'.$user->emp[3].'-01';
        }
        $user_type[] = $user->emp;
        $user_type[] = $user;
        return response()->json($user_type);
    }

    public function AccountantGetLevelType(Request $request){
        $type = UserType::where('title', $request->input('user_type'))->get()->first();
        // dd($type);
        $type = $type->level;
        return response()->json(['level' => $type]);
    }

    public function AccountantCalculator(Request $request){
        $arr = $request->all();
        $calculator = new CalculatorSalary($arr);
        return response()->json($calculator->toString());
    }

    public function AccountantSaveNoty(Request $request){
        $user_id = $request->input('account_id');
        $data = $request->input('data');
        $user = User::where('id', $user_id)->get()->first();
        if(SalaryStoreAccontant::where('user_id_salary', $user_id)->whereYear('created_at', '=', now()->year)->whereMonth('created_at', '=', now()->month)->count() == 0){
            $salary = new SalaryStoreAccontant();
            $salary->user_id_salary = $user_id;
            $salary->thanh_tien = $data['thanh_tien'];
            $salary->he_so_ht = $data['he_so_ht'];
            $salary->luong_tham_nien = $data['luong_tham_nien'];
            $salary->luong_le_phep = $data['luong_le_phep'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_bao_hiem = $data['khau_tru_bao_hiem'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }else{
            $salary = SalaryStoreAccontant::where('user_id_salary',$user_id)->first();
            $salary = SalaryStoreAccontant::find($salary->id);
            $salary->thanh_tien = $data['thanh_tien'];
            $salary->he_so_ht = $data['he_so_ht'];
            $salary->luong_tham_nien = $data['luong_tham_nien'];
            $salary->luong_le_phep = $data['luong_le_phep'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_bao_hiem = $data['khau_tru_bao_hiem'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }
        

        SendEmail::dispatch($user, $user->email, [
            'title' => '('.$user->username.')Lương của bạn tháng '.date('Y-m-d'),
            'body' => 'Lương thực nhận: '.$data["thuc_nhan"].' VND. Truy cập <a href="'.route('home_index').'">'.route('home_index').'</a> để xem chi tiết',
            'image' => 'https://raw.githubusercontent.com/lime7/responsive-html-template/master/index/intro__bg.png'
            ]);
        $phone = $user->phone;
        $phone = new PhoneSms($user->username.' - Muc luong thuc nhan: '.$data['thuc_nhan'].'VND. Duoc khoi tao luc '.date('Y-m-d').'. Truy cap tai day de xem day du hon '.route('home_index'), $phone);
        $phone->sendMessage();
        return response()->json(['status' => true]);
    }

    public function AccountantSavewithoutSaveNoty(Request $request){
        $user_id = $request->input('account_id');
        $data = $request->input('data');
        $user = User::where('id', $user_id)->get()->first();
        if(SalaryStoreAccontant::where('user_id_salary', $user_id)->whereYear('created_at', '=', now()->year)->whereMonth('created_at', '=', now()->month)->count() == 0){
            $salary = new SalaryStoreAccontant();
            $salary->user_id_salary = $user_id;
            $salary->thanh_tien = $data['thanh_tien'];
            $salary->he_so_ht = $data['he_so_ht'];
            $salary->luong_tham_nien = $data['luong_tham_nien'];
            $salary->luong_le_phep = $data['luong_le_phep'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_bao_hiem = $data['khau_tru_bao_hiem'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }else{
            $salary = SalaryStoreAccontant::where('user_id_salary',$user_id)->first();
            $salary = SalaryStoreAccontant::find($salary->id);
            $salary->thanh_tien = $data['thanh_tien'];
            $salary->he_so_ht = $data['he_so_ht'];
            $salary->luong_tham_nien = $data['luong_tham_nien'];
            $salary->luong_le_phep = $data['luong_le_phep'];
            $salary->tong_thu_nhap = $data['tong_thu_nhap'];
            $salary->tong_thu_nhap_chiu_thue = $data['tong_thu_nhap_chiu_thue'];
            $salary->khau_tru_bao_hiem = $data['khau_tru_bao_hiem'];
            $salary->khau_tru_thue_tncn = $data['khau_tru_thue_tncn'];
            $salary->BHXH = $data['bao_hiem']['BHXH'];
            $salary->BHYT = $data['bao_hiem']['BHYT'];
            $salary->BHTN = $data['bao_hiem']['BHTN'];
            $salary->thuc_nhan = $data['thuc_nhan'];
            $salary->save();
        }
        return response()->json(['status' => true]);
        
    }

}
