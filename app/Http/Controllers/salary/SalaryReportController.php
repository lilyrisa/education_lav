<?php

namespace App\Http\Controllers\salary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SalaryStoreAccontant;
use App\Models\SalaryStore;
use App\Models\UserType;
use App\User;
use App\Repositories\UserTypeRepo;
use response;

class SalaryReportController extends Controller
{
    protected $user_type;

    public function __construct(UserTypeRepo $user_type)
    {
        $this->user_type = $user_type;
    }

    public function index(){
        $user_type = UserType::where('level', 3)->get();
        $lsUser = $this->user_type->getListUserByType($user_type);
        return view('pages.support_team.salary_calculate.salary_report',['lsUser' => $lsUser]);
    }

    public function ListUser(Request $request){
        $user_type = $request->input('user');
        $user_type = UserType::where('level', $user_type)->get();
        $lsUser = $this->user_type->getListUserByType($user_type);
        return response()->json($lsUser);
    }

    public function ajax(Request $request){
        $type = (int)$request->input('type_u');
        $year = $request->input('year');
        $month = $request->input('month');
        $user = $request->input('user');
        $user = User::find($user);
        if($type == 3){
            $data = SalaryStore::with('user')->where('user_id_salary', $request->input('user'))->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->get()->toArray();
            foreach($data as &$item){
                $item['user'] = $user;
            }
            return response()->json($data);
        }
        $data = SalaryStoreAccontant::with('user')->where('user_id_salary', $request->input('user'))->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->get()->toArray();
        foreach($data as &$item){
            $item['user'] = $user;
        }
        return response()->json($data);
    }
}
