<?php

namespace App\Http\Controllers\salary;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Qs;

use App\Repositories\SalaryParameterRepo;
use App\Repositories\UserTypeRepo;
use App\Models\UserType;
use App\Http\Requests\Salary\SalaryParameter;
use App\Http\Requests\Salary\SalaryParameterUpdate;


class SalaryParameterController extends Controller
{
    protected $salary, $user_type;

    public function __construct(SalaryParameterRepo $salary, UserTypeRepo $user_type)
    {
        $this->salary = $salary;
        $this->user_type = $user_type;

    }

    public function index(){
        $user_type = UserType::where('level', 3)->orWhere('level', 5)->get();
        $lsUser = $this->user_type->getListUserByType($user_type);
        $salary = $this->salary->getUser();
        // dd($salary);
        return view('pages.support_team.salary_parameter.index',['lsUser' => $lsUser, 'salary' => $salary]);
    }

    public function create(SalaryParameter $reg){
        $data = $reg->all();
        $this->salary->create($data);
        return Qs::jsonStoreOk();
    }

    public function getupdate($salary_id){
        $salary = $this->salary->find($salary_id);
        return view('pages.support_team.salary_parameter.edit',['salary' => $salary]);
    }

    public function update(SalaryParameterUpdate $reg, $salary_id){
        $data = $reg->all();
        $this->salary->update($salary_id, $data);
        return back()->with('flash_success', __('msg.update_ok'));
    }

}
