<?php

namespace App\Http\Controllers;

use App\Helpers\Qs;
use App\Helpers\CovidApi;
use App\Repositories\UserRepo;
use Illuminate\Http\Request;
use App\Models\UserType;

class HomeController extends Controller
{
    protected $user;
    public function __construct(UserRepo $user)
    {
        $this->user = $user;
    }


    public function index()
    {
        return redirect()->route('dashboard');
    }

    public function privacy_policy()
    {
        $data['app_name'] = config('app.name');
        $data['app_url'] = config('app.url');
        $data['contact_phone'] = Qs::getSetting('phone');
        return view('pages.other.privacy_policy', $data);
    }

    public function terms_of_use()
    {
        $data['app_name'] = config('app.name');
        $data['app_url'] = config('app.url');
        $data['contact_phone'] = Qs::getSetting('phone');
        return view('pages.other.terms_of_use', $data);
    }

    public function dashboard()
    {
        $d=[];
        if(Qs::userIsTeamSAT()){
            $d['users'] = $this->user->getAll();
        }
        $accountant = 0;
        $teacher = 0;
        $match_acc = UserType::where('level', 5)->select('title')->get()->toArray();
        $match_tc = UserType::where('level', 3)->select('title')->get()->toArray();
        $arr_acc = [];
        $arr_tc = [];
        foreach($match_acc as $acc){
            $arr_acc[] = $acc['title'];
        }
        foreach($match_tc as $tc){
            $arr_tc[] = $tc['title'];
        }
        foreach($d['users'] as $item){
            if(in_array($item->user_type,$arr_acc)){
                $accountant++;
            }
            if(in_array($item->user_type,$arr_tc)){
                $teacher++;
            }
        }
        $d['accountant'] = $accountant;
        $d['teacher'] = $teacher;
        return view('pages.support_team.dashboard', $d);
    }

    public function covid_api(Request $request){
        $lastday = $request->input('last_day');
        $covid = CovidApi::get_all_data_from_day($lastday);
        return \Response()->json($covid);
    }

    public function ladingpage(){
        return view('ladingpage');
    }
}
