<?php

namespace App\Http\Requests\TypeUser;

use App\Helpers\Qs;
use Illuminate\Foundation\Http\FormRequest;

class TeacherUser extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3',
            'name' => 'required',
            'level' => 'required',
        ];
    }

    public function attributes()
    {
        return  [
            'title' => 'title',
            'name' => 'name',
            'level' => 'level',
        ];
    }

    // protected function getValidatorInstance()
    // {
    //     $input = $this->all();

    //     $input['teacher_id'] = $input['teacher_id'] ? Qs::decodeHash($input['teacher_id']) : NULL;

    //     $this->getInputSource()->replace($input);

    //     return parent::getValidatorInstance();
    // }
}
