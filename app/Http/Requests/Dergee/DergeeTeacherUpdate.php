<?php

namespace App\Http\Requests\Dergee;

use App\Helpers\Qs;
use Illuminate\Foundation\Http\FormRequest;

class DergeeTeacherUpdate extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type_id' => 'required',
            'dergee_name' => 'required',
        ];
    }

    public function attributes()
    {
        return  [
            'user_type_id' => 'required',
            'dergee_name' => 'required',
        ];
    }

    protected function getValidatorInstance()
    {
        $input = $this->all();

        $input['user_type_id'] = $input['user_type_id'] ? Qs::decodeHash($input['user_type_id']) : NULL;

        $this->getInputSource()->replace($input);

        return parent::getValidatorInstance();
    }
}
