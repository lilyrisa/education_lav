<?php

namespace App\Http\Requests\Salary;

use App\Helpers\Qs;
use Illuminate\Foundation\Http\FormRequest;

class SalaryParameterUpdate extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id_para' => 'required',
        ];
    }

    public function attributes()
    {
        return  [
            'user_id_para' => 'required',
            'xep_loai' => 'required',
            'ngay_cong' => 'required',
            'don_gia_dich_vu' => 'required',
            'an_ca' => 'required',
            'tien_dien_thoai' => 'required',
            'he_so_pc_kiem_nhiem' => 'required',
            'phu_cap_uu_dai' => 'required',
            'muc_tham_gia_bao_hiem' => 'required',
        ];
    }

    protected function getValidatorInstance()
    {
        $input = $this->all();

        $input['user_id_para'] = $input['user_id_para'] ? Qs::decodeHash($input['user_id_para']) : NULL;

        $this->getInputSource()->replace($input);

        return parent::getValidatorInstance();
    }
}
