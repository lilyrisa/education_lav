<div style="margin-bottom: 5px; text-align: center">
    <table border="0" cellpadding="5" cellspacing="5" style="text-align: center; margin: 0 auto;">
        <tr>
            <td><strong>TỪ KHÓA LÊN LỚP</strong></td>
            @if(Mk::getGradeList($class_type->id)->count())
                @foreach(Mk::getGradeList($class_type->id) as $gr)
                    <td><strong>{{ $gr->name }}</strong>
                        => {{ $gr->mark_from.' - '.$gr->mark_to }}
                    </td>
                @endforeach
            @endif
        </tr>
    </table>

</div>


<table style="width:100%; border-collapse:collapse; ">
    <tbody>
    <tr>
        <td><strong>SỐ : </strong></td>
        <td><strong>Cách biệt:</strong> {{ Mk::countDistinctions($marks) }}</td>
        <td><strong>Số dư:</strong> {{ Mk::countCredits($marks) }}</td>
        <td><strong>Vượt qua:</strong> {{ Mk::countPasses($marks) }}</td>
        <td><strong>Thất bại:</strong> {{ Mk::countFailures($marks) }}</td>
        <td><strong>Đối tượng được cung cấp:</strong> {{ Mk::countSubjectsOffered($marks) }}</td>
    </tr>

    </tbody>
</table>
