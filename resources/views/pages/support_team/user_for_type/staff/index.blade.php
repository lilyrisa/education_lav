@extends('layouts.master')
@section('page_title', 'Quản lý nhân viên')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Quản lý nhân viên</h6>
        </div>

        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#new-subject" class="nav-link active" data-toggle="tab">Thêm chức vụ nhân viên</a></li>
                <li class="nav-item dropdown">
                    <a href="#list" class="dropdown-item" data-toggle="tab">Quản lý nhân viên</a>
                    {{-- <div class="dropdown-menu dropdown-menu-right">
                            <a href="#list" class="dropdown-item" data-toggle="tab">Danh sách nhân viên</a>
                    </div> --}}
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('typeuser.save_staff') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Tên chức vụ <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="name" name="name" required type="text" class="form-control" placeholder="Tên chức vụ">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Mã chức vụ <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="title" name="title" required type="text" class="form-control" placeholder="Mã chức vụ">
                                    </div>
                                </div>
                                <input id="level" name="level" required type="text" hidden value="5">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                    <div class="tab-pane fade" id="list">                         <table class="table datatable-button-html5-columns">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Mã chức vụ</th>
                                <th>Tên chức vụ</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user_type as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    <td>{{ $s->title }} </td>
                                    <td>{{ $s->name }} </td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-left">
                                                   
                                                    @if(Qs::userIsTeamSA())
                                                        <a href="{{ route('typeuser.get_update_staff', $s->id) }}" class="dropdown-item"><i class="icon-pencil"></i> Sửa</a>
                                                    @endif
                                                    
                                                    @if(Qs::userIsSuperAdmin())
                                                        <a id="{{ $s->id }}" onclick="confirmDelete(this.id)" href="#" class="dropdown-item"><i class="icon-trash"></i> Xóa</a>
                                                        <form method="post" id="item-delete-{{ $s->id }}" action="{{ route('typeuser.destroy_staff', $s->id) }}" class="hidden">@csrf @method('delete')</form>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
