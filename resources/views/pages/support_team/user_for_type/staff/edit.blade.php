@extends('layouts.master')
@section('page_title', 'Sửa chức vụ')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Sửa chức vụ</h6>
        </div>

        <div class="card-body">
        

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('typeuser.update_staff', ['u_id' => $user_type->id]) }}">
                                @csrf @method('PUT')
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Tên chức vụ <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="name" name="name" required type="text" class="form-control" value="{{$user_type->name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Mã chức vụ <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="title" name="title" required type="text" class="form-control" value="{{$user_type->title}}">
                                    </div>
                                </div>
                                <input id="level" name="level" required type="text" hidden value="5">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
