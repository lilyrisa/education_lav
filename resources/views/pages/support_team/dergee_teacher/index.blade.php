@extends('layouts.master')
@section('page_title', 'Quản lý học vị giảng viên')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Quản lý học vị giảng viên</h6>
        </div>

        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#new-subject" class="nav-link active" data-toggle="tab">Thêm học vị giảng viên</a></li>
                <li class="nav-item dropdown">
                    <a href="#list" class="dropdown-item" data-toggle="tab">Quản lý học vị giảng viên</a>
                    {{-- <div class="dropdown-menu dropdown-menu-right">
                            <a href="#list" class="dropdown-item" data-toggle="tab">Danh sách nhân viên</a>
                    </div> --}}
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('dergee.save') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="teacher_id" class="col-lg-3 col-form-label font-weight-semibold">Giảng viên</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Chọn giáo viên" class="form-control select-search" name="user_type_id" id="user_type_id ">
                                            <option value=""></option>
                                            @foreach($user_type as $ust)
                                                <option value="{{ Qs::hash($ust->id) }}">{{ $ust->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                
                                        <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Học vị giảng viên <span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input id="dergee_name" name="dergee_name" required type="text" class="form-control" placeholder="Tên chức vụ">
                                        </div>
                                  
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                    <div class="tab-pane fade" id="list">                         <table class="table datatable-button-html5-columns">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Loại giảng viên</th>
                                <th>Học vị giảng viên</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dergee as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    {{-- @if($s->teacher_id)
                                    <td><a target="_blank" href="{{ route('users.show', Qs::hash($s->teacher_id)) }}">{{ $s->teacher->name }}</a></td>
                                        @else
                                        <td> - </td>
                                    @endif --}}
                                    <td>{{ $s->user_type->name }}</td>
                                    <td>{{ $s->dergee_name }} </td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-left">
                                                   
                                                    @if(Qs::userIsTeamSA())
                                                        <a href="{{ route('dergee.get_update', $s->id) }}" class="dropdown-item"><i class="icon-pencil"></i> Sửa</a>
                                                    @endif
                                                    
                                                    @if(Qs::userIsSuperAdmin())
                                                        <a id="{{ $s->id }}" onclick="confirmDelete(this.id)" href="#" class="dropdown-item"><i class="icon-trash"></i> Xóa</a>
                                                        <form method="post" id="item-delete-{{ $s->id }}" action="{{ route('dergee.destroy', $s->id) }}" class="hidden">@csrf @method('delete')</form>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
