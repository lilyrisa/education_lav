@extends('layouts.master')
@section('page_title', 'Sửa học vị giảng viên')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Sửa học vị giảng viên</h6>
        </div>

        <div class="card-body">
        

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('dergee.update', ['u_id' => $dergee->id]) }}">
                                @csrf @method('PUT')
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Tên học vị giảng viên <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Chọn giáo viên" class="form-control select-search" name="user_type_id" id="user_type_id">
                                            <option value=""></option>
                                            @foreach($user_type as $ust)
                                                <option {{$ust->id == $dergee->user_type_id ? 'selected': ''}} value="{{ Qs::hash($ust->id) }}">{{ $ust->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-lg-3 col-form-label font-weight-semibold">Học vị giảng viên <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="dergee_name" name="dergee_name" required type="text" class="form-control" value="{{$dergee->dergee_name}}">
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
