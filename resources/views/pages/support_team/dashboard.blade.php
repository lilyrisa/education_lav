@extends('layouts.master')
@section('page_title', 'My Dashboard')

@section('head')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<style>
    .loading {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: rgba(0,0,0,.5);
}
.loading-wheel {
    width: 20px;
    height: 20px;
    margin-top: -40px;
    margin-left: -40px;
    
    position: absolute;
    top: 50%;
    left: 50%;
    
    border-width: 30px;
    border-radius: 50%;
    -webkit-animation: spin 1s linear infinite;
}
.style-2 .loading-wheel {
    border-style: double;
    border-color: #ccc transparent;
}
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0);
    }
    100% {
        -webkit-transform: rotate(-360deg);
    }
}
</style>
@endsection
@section('content')

    @if(Qs::userIsTeamSA())
       <div class="row">
           <div class="col-sm-6 col-xl-2">
               <div class="card card-body bg-blue-400 has-bg-image">
                   <div class="media">
                       <div class="media-body">
                           <h3 class="mb-0">{{ $users->where('user_type', 'student')->count() }}</h3>
                           <span class="text-uppercase font-size-xs font-weight-bold">Tổng số học sinh</span>
                       </div>

                       <div class="ml-3 align-self-center">
                           <i class="icon-users4 icon-3x opacity-75"></i>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-2">
               <div class="card card-body bg-danger-400 has-bg-image">
                   <div class="media">
                       <div class="media-body">
                           <h3 class="mb-0">{{ $teacher }}</h3>
                           <span class="text-uppercase font-size-xs">Tổng số giáo viên</span>
                       </div>

                       <div class="ml-3 align-self-center">
                           <i class="icon-users2 icon-3x opacity-75"></i>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-2">
            <div class="card card-body bg-warning-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{ $accountant }}</h3>
                        <span class="text-uppercase font-size-xs">Tổng số nhân viên</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-users2 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

           <div class="col-sm-6 col-xl-2">
               <div class="card card-body bg-success-400 has-bg-image">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                           <i class="icon-pointer icon-3x opacity-75"></i>
                       </div>

                       <div class="media-body text-right">
                           <h3 class="mb-0">{{ $users->where('user_type', 'admin')->count() }}</h3>
                           <span class="text-uppercase font-size-xs">Tổng số quản trị viên</span>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-2">
               <div class="card card-body bg-indigo-400 has-bg-image">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                           <i class="icon-user icon-3x opacity-75"></i>
                       </div>

                       <div class="media-body text-right">
                           <h3 class="mb-0">{{ $users->where('user_type', 'parent')->count() }}</h3>
                           <span class="text-uppercase font-size-xs">Tổng số phụ huynh</span>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       @endif

       <div class="card">
        <div class="card-header header-elements-inline">
            <h2>Dữ liệu dịch bệnh covid-19 tại Việt Nam<br/><small>Dữ liệu được lấy từ nguồn thứ 3</small></h2>
                <div class="row">
                    <div class="col col-md-4">
                        <label for="">Số ngày trước</label>
                        <input type="number" class="form-control" id="lastday" value="120">
                    </div>
                    <div class="col col-md-2">
                        <button class="btn btn-warning" style="margin-top: 30px" id="load_data">Trích xuất</button>
                    </div>
                </div>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more-vert"></i> </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
        </div>

        <div class="card-body">
            <div id="area_chart" class="graph"></div>
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Lịch sự kiện của trường<br/><p style="color: red; font-size: 20px">Hiện tại chức năng chưa có sẵn</p></h5>
         {!! Qs::getPanelOptions() !!}
        </div>

        <div class="card-body">
            <div class="fullcalendar-basic"></div>
        </div>
    </div>
    <div class="loading style-2" id="loading"><div class="loading-wheel"></div></div>
    @endsection

@section('scripts')
<script>
    var morrisLine;
    initMorris();
    
    function setMorris(data) {
      morrisLine.setData(data);
      morrisLine.redraw();
    }
    function initMorris(data) {
        morrisLine = Morris.Line({
            element: 'area_chart',
            // data: data,
            lineColors: ['#ffcf4e', 'red', 'green'],
            xkey: 'date',
            ykeys: ['cases', 'deaths', 'recovered'],
            labels: ['Số ca', 'Tử vong', 'Hồi phục'],
            pointSize: 2,
            lineWidth: 1,
            resize: true,
            grid: true,
            axes: true,
            fillOpacity: 0.5,
            behaveLikeLine: true,
            gridLineColor: '#e3e5e7',
            hideHover: 'auto'
        });
    }
$(document).ready(function() {
    
    $.ajax({
        url: "{{route('covid_api')}}",
        type: 'POST',
        data:{
            last_day: 120,
            "_token": "{{ csrf_token() }}",
        }
    }).done(res => {
        $('#loading').hide();
        console.log(res.timeline);
        var cases = res.timeline.cases;
        var deaths = res.timeline.deaths;
        var recovered = res.timeline.recovered;
        var data_covid = [];
        for(let index in cases) { 
            let obj = {};
            obj.date = index;
            obj.cases = cases[index];
            obj.deaths = deaths[index];
            obj.recovered = recovered[index];
            data_covid.push(obj);
        }
        setMorris(data_covid);
    })
    $('#load_data').on('click', function(e){
        e.preventDefault();
        $('#loading').show();
        var last_day = $('#lastday').val();
        $.ajax({
            url: "{{route('covid_api')}}",
            type: 'POST',
            data:{
                last_day: last_day,
                "_token": "{{ csrf_token() }}",
            }
        }).done(res => {
            $('#loading').hide();
            // console.log(res.timeline);
            var cases = res.timeline.cases;
            var deaths = res.timeline.deaths;
            var recovered = res.timeline.recovered;
            var data_covid = [];
            for(let index in cases) { 
                let obj = {};
                obj.date = index;
                obj.cases = cases[index];
                obj.deaths = deaths[index];
                obj.recovered = recovered[index];
                data_covid.push(obj);
            }
            setMorris(data_covid);
        })
    })
})
</script>
@endsection