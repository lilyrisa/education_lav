@extends('layouts.master')
@section('page_title', 'Sửa tham số tính lương')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Sửa tham số tính lương</h6>
        </div>

        <div class="card-body">
        

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('para.update_ajax', ['salary_id' => $salary->id]) }}">
                                @csrf @method('PUT')
                                <div class="form-group row">
                                    <label for="user_id_para" class="col-lg-3 col-form-label font-weight-semibold">Nhân viên nhà trường</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="user_id_para" id="user_id_para" value="{{Qs::hash($salary->user->id)}}" hidden>
                                        <select class="form-control select-search"  disabled="true">
                                            <option value="{{Qs::hash($salary->user->id)}}" selected>{{$salary->user->name}}</option>
                                        </select>
                                    </div>
                                
                                    <label for="xep_loai" class="col-lg-3 col-form-label font-weight-semibold">Xếp loại</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Xếp loại" class="form-control select-search" name="xep_loai" id="xep_loai">
                                            @if($salary->xep_loai == 'A')
                                                <option value="A" selected>A</option>
                                                <option value="B" >B</option>
                                                <option value="C" >C</option>
                                                <option value="D" >D</option>
                                            @endif
                                            @if($salary->xep_loai == 'B')
                                                <option value="A" >A</option>
                                                <option value="B" selected>B</option>
                                                <option value="C" >C</option>
                                                <option value="D" >D</option> 
                                            @endif
                                            @if($salary->xep_loai == 'C')
                                                <option value="A" >A</option>
                                                <option value="B" >B</option>
                                                <option value="C" selected>C</option>
                                                <option value="D" >D</option> 
                                            @endif
                                            @if($salary->xep_loai == 'D')
                                                <option value="A" >A</option>
                                                <option value="B" >B</option>
                                                <option value="C" >C</option>
                                                <option value="D" selected>D</option> 
                                            @endif
                                        </select>
                                    </div>

                                    <label for="ngay_cong" class="col-lg-3 col-form-label font-weight-semibold">Ngày công thực tế</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="ngay_cong" id="ngay_cong" value="{{$salary->ngay_cong}}">
                                    </div>

                                    <label for="don_gia_dich_vu" class="col-lg-3 col-form-label font-weight-semibold">Đơn giá dịch vụ</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="don_gia_dich_vu" id="don_gia_dich_vu" value="{{$salary->don_gia_dich_vu}}">
                                    </div>

                                    <label for="an_ca" class="col-lg-3 col-form-label font-weight-semibold">Đơn giá ăn ca</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="an_ca" id="an_ca" value="{{$salary->an_ca}}">
                                    </div>

                                    <label for="tien_dien_thoai" class="col-lg-3 col-form-label font-weight-semibold">Tiền điện thoại</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="tien_dien_thoai" id="tien_dien_thoai" value="{{$salary->tien_dien_thoai}}">
                                    </div>

                                    <label for="he_so_pc_kiem_nhiem" class="col-lg-3 col-form-label font-weight-semibold">Hệ số PC kiêm nhiệm</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="he_so_pc_kiem_nhiem" id="he_so_pc_kiem_nhiem" min="0.1" step="any" value="{{$salary->he_so_pc_kiem_nhiem}}">
                                    </div>

                                    <label for="phu_cap_uu_dai" class="col-lg-3 col-form-label font-weight-semibold">Phụ cấp ưu đãi</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="phu_cap_uu_dai" id="phu_cap_uu_dai" value="{{$salary->phu_cap_uu_dai}}">
                                    </div>

                                    <label for="muc_tham_gia_bao_hiem" class="col-lg-3 col-form-label font-weight-semibold">Mức tham gia bảo hiểm (VND)</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="muc_tham_gia_bao_hiem" id="muc_tham_gia_bao_hiem" value="{{$salary->muc_tham_gia_bao_hiem}}">
                                    </div>
                                  
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
