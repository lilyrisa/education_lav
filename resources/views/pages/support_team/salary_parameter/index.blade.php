@extends('layouts.master')
@section('page_title', 'Quản lý tham số tính lương')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Quản lý tham số tính lương</h6>
        </div>

        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#new-subject" class="nav-link active" data-toggle="tab">Thêm tham số tính lương</a></li>
                <li class="nav-item dropdown">
                    <a href="#list" class="dropdown-item" data-toggle="tab">Quản lý tham số tính lương</a>
                    {{-- <div class="dropdown-menu dropdown-menu-right">
                            <a href="#list" class="dropdown-item" data-toggle="tab">Danh sách nhân viên</a>
                    </div> --}}
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-subject">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="ajax-store" method="post" action="{{ route('para.create') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="user_id_para" class="col-lg-3 col-form-label font-weight-semibold">Nhân viên nhà trường</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Chọn nhân viên trường" class="form-control select-search" name="user_id_para" id="user_id_para">
                                            <option value=""></option>
                                            @foreach($lsUser as $item)
                                                <option value="{{ Qs::hash($item->id) }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                
                                    <label for="xep_loai" class="col-lg-3 col-form-label font-weight-semibold">Xếp loại</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Xếp loại" class="form-control select-search" name="xep_loai" id="xep_loai">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                        </select>
                                    </div>

                                    <label for="ngay_cong" class="col-lg-3 col-form-label font-weight-semibold">Ngày công thực tế</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="ngay_cong" id="ngay_cong">
                                    </div>

                                    <label for="don_gia_dich_vu" class="col-lg-3 col-form-label font-weight-semibold">Đơn giá dịch vụ</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="don_gia_dich_vu" id="don_gia_dich_vu">
                                    </div>

                                    <label for="an_ca" class="col-lg-3 col-form-label font-weight-semibold">Đơn giá ăn ca</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="an_ca" id="an_ca">
                                    </div>

                                    <label for="tien_dien_thoai" class="col-lg-3 col-form-label font-weight-semibold">Tiền điện thoại</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="tien_dien_thoai" id="tien_dien_thoai">
                                    </div>

                                    <label for="he_so_pc_kiem_nhiem" class="col-lg-3 col-form-label font-weight-semibold">Hệ số PC kiêm nhiệm</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="he_so_pc_kiem_nhiem" id="he_so_pc_kiem_nhiem" min="0.1" step="any">
                                    </div>

                                    <label for="phu_cap_uu_dai" class="col-lg-3 col-form-label font-weight-semibold">Phụ cấp ưu đãi</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="phu_cap_uu_dai" id="phu_cap_uu_dai">
                                    </div>

                                    <label for="muc_tham_gia_bao_hiem" class="col-lg-3 col-form-label font-weight-semibold">Mức tham gia bảo hiểm (VND)</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="muc_tham_gia_bao_hiem" id="muc_tham_gia_bao_hiem">
                                    </div>
                                  
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                    <div class="tab-pane fade" id="list">                         
                        <table class="table datatable-button-html5-columns">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Nhân viên</th>
                                <th>Xếp loại</th>
                                <th>Ngày công</th>
                                <th>Đơn giá dịch vụ</th>
                                <th>Đơn giá ăn ca</th>
                                <th>Tiền điện thoại</th>
                                <th>Hệ số PC kiêm nhiệm</th>
                                <th>Phụ cấp ưu đãi</th>
                                <th>Mức tham gia bảo hiểm</th>
                                <th>Khởi tạo</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($salary as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    <td>{{$s->user->name}}</td>
                                    <td>{{$s->xep_loai}}</td>
                                    <td>{{$s->ngay_cong}}</td>
                                    <td>{{number_format($s->don_gia_dich_vu,0).' VND'}}</td>
                                    <td>{{number_format($s->an_ca,0).' VND'}}</td>
                                    <td>{{number_format($s->tien_dien_thoai,0).' VND'}}</td>
                                    <td>{{$s->he_so_pc_kiem_nhiem}}</td>
                                    <td>{{$s->phu_cap_uu_dai}}</td>
                                    <td>{{number_format($s->muc_tham_gia_bao_hiem,0).' VND'}}</td>
                                    <td>{{$s->created_at}}</td>
                                    
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-left">
                                                   
                                                    @if(Qs::userIsTeamSA())
                                                        <a href="{{ route('para.update', $s->id) }}" class="dropdown-item"><i class="icon-pencil"></i> Sửa</a>
                                                    @endif
                                                    
                                                    @if(Qs::userIsSuperAdmin())
                                                        <a id="{{ $s->id }}" onclick="confirmDelete(this.id)" href="#" class="dropdown-item"><i class="icon-trash"></i> Xóa</a>
                                                        <form method="post" id="item-delete-{{ $s->id }}" action="{{ route('dergee.destroy', $s->id) }}" class="hidden">@csrf @method('delete')</form>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection
