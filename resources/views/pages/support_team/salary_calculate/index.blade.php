@extends('layouts.master')
@section('page_title', 'Tính lương cho giảng viên')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Tính lương cho giảng viên</h6>
            {!! Qs::getPanelOptions() !!}
        </div>

        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-salary">
                    <div class="row">
                        <div class="col-md-12">
                            <form>
                            <div class="row" id="form">
                                <div class="form-group col-md-2 account">
                                    <label for="staff_id" class="col-lg-12 col-form-label font-weight-semibold">Giảng viên và nhân viên <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="Chọn giảng viên" class="form-control select-search" name="staff_id" id="staff_id">
                                            <option value="">Chọn nhân viên</option>
                                            @foreach($lsUser as $u_item)
                                            <option value="{{$u_item->id}}" data-user_type="{{$u_item->user_type}}" data-level="{{$u_item->level_type}}">{{$u_item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="position" class="col-lg-12 col-form-label font-weight-semibold">Chức vụ <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="chức vụ giảng viên" class="form-control" name="staff_id" id="position">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="ngaykihopdong" class="col-lg-12 col-form-label font-weight-semibold">Ngày kí hợp đồng <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="ngaykihopdong" name="ngaykihopdong" required type="date" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="xeploai" class="col-lg-12 col-form-label font-weight-semibold">Xếp loại <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="Xếp loại" class="form-control" name="xeploai" id="xeploai">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="ngaycong" class="col-lg-12 col-form-label font-weight-semibold">Ngày công <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="ngaycong" name="ngaycong" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="quymosv" class="col-lg-12 col-form-label font-weight-semibold">Quy mô sinh viên trực thuộc <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="quymosv" name="quymosv" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="kmp" class="col-lg-12 col-form-label font-weight-semibold">KMP bình quân tháng <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="kmp" name="kmp" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="sotietquydoi" class="col-lg-12 col-form-label font-weight-semibold">Số tiết quy đổi thực tế <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="sotietquydoi" name="sotietquydoi" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="anca" class="col-lg-12 col-form-label font-weight-semibold">Đơn giá ăn ca <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="anca" name="anca" required type="text" class="form-control price">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="dienthoai" class="col-lg-12 col-form-label font-weight-semibold">Tiền điện thoại<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="dienthoai" name="dienthoai" required type="text" class="form-control price">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="mucthamgiabh" class="col-lg-12 col-form-label font-weight-semibold">Mức tham gia bảo hiểm (VND)<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="mucthamgiabh" name="mucthamgiabh" required type="text" class="form-control price">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="dongiadv" class="col-lg-12 col-form-label font-weight-semibold">Đơn giá dịch vụ (VND)<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="dongiadv" name="dongiadv" required type="text" class="form-control price">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="KLGD" class="col-lg-12 col-form-label font-weight-semibold">Hệ số HD KLGD<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="KLGD" name="KLGD" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="hesoquymosinhvien" class="col-lg-12 col-form-label font-weight-semibold">Hệ số quy mô sinh viên<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="hesoquymosinhvien" name="hesoquymosinhvien" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="hesoquypckiem" class="col-lg-12 col-form-label font-weight-semibold">Hệ số pc kiêm nhiệm<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="hesoquypckiem" name="hesoquypckiem" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="heso" class="col-lg-12 col-form-label font-weight-semibold">Hệ số<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="heso" name="heso" required type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="pcuudai" class="col-lg-12 col-form-label font-weight-semibold">Phụ cấp ưu đãi<span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input id="pcuudai" name="pcuudai" required type="text" class="form-control price">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary" id="tinhluong">Tính lương <i class="icon-paperplane ml-2"></i></button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content" id="kqtv" style="display:none">
                <span class="font-weight-semibold">Kết quả trả về</span> 
                <table class="table table-condensed table-hover">
    			<thead>
    				<tr>
    					<th> </th>
    					<th> </th>
    					<th>Thành tiền</th>
    				</tr>
    			</thead>
    			<tbody>
    				<tr>
                        <td></td>
    					<td>Khối lượng tiết giảng khoa</td>
    					<td class="priceresp"></td>
    				</tr>
    				<tr style="background-color: #04ff0017">
    					<td rowspan="3">Khối lượng theo tiết giảng</td>
    					<td>Đơn giá tiết giảng</td>
    					<td class="priceresp"></td>

    				</tr>
    				<tr style="background-color: #04ff0017">
    					<td>Đơn giá phụ cấp tiết giảng</td>
    					<td class="priceresp"></td>
    				</tr>
    				<tr style="background-color: #04ff0017">
    					<td>Thành tiền</td>
    					<td class="priceresp"></td>
    				</tr>
    				<tr>
    					<td></td>
    					<td>Phụ cấp đứng lớp</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr>
    					<td></td>
    					<td>Lương quản lý phụ cấp khác</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr>
    					<td></td>
    					<td>Phụ cấp thâm niên</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr >
    					<td></td>
    					<td style="background-color: #ff000017">Tổng thu nhập</td>
    					<td class="priceresp" style="background-color: #ff000017"></td>
    				</tr>
                    <tr>
    					<td></td>
    					<td>Tổng thu nhập chịu thuế</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr>
    					<td></td>
    					<td>Khấu trừ thuế thu nhập cá nhân</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr style="background-color: #2700ff17">
    					<td rowspan="3">Bảo hiểm</td>
    					<td>Bảo hiểm xã hội</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr style="background-color: #2700ff17">
    					<td>Bảo hiểm y tế</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr style="background-color: #2700ff17">
    					<td>Bảo hiểm thất nghiệp</td>
    					<td class="priceresp"></td>

    				</tr>
                    <tr style="background-color: #10ff005e">
    					<td ><b>Thực nhận</b></td>
                        <td></td>
    					<td class="priceresp"></td>
    				</tr>
    				<tr>
    					<td> </td>
    					<td><a class="btn btn-success" href="#" id="save"><i class="icon-shopping-cart icon-white"></i> Lưu vào database với tháng hiện tại và gửi thông báo</a></td>
    					<td><a class="btn btn-primary" href="#" id="savewithout"><i class="icon-shopping-cart icon-white"></i> Lưu vào database với tháng hiện tại không gửi thông báo</a></td>
    				</tr>
    			</tbody>
    		</table>
            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var user = {};
    // get user_type
    $('#staff_id').on('change', function(){
        var id = $(this).val();
        var type = $(this).find('option:selected').data('user_type');
        var level = $(this).find('option:selected').data('level');
        console.log(level);
        // $('#form').insertBefore('#hocvi');
        if(level != ''){
            $('#hocvi').remove();
            var tmp = `
                    <div class="form-group col-md-2" id="hocvi">
                        <label for="level" class="col-lg-12 col-form-label font-weight-semibold">Học vị <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required data-placeholder="học vị" class="form-control" id="level">
                                <option value="${level}">${level}</option>
                            </select>
                        </div>
                    </div>
                `;
                $(tmp).insertAfter('.account');
        }else{
            $('#hocvi').remove();
        }
        // get position
        $.ajax({
            url:'{{route("salary.get_position_user")}}',
            type: 'post',
            data: {
                user_id: id,
                "_token": "{{ csrf_token() }}",
            }
        }).done(resp => {
            console.log(resp);
            let emp = resp[1];
            let para = resp[2];
            resp = resp[0];
            $('#position').html(`
                <option value="${resp.id}">${resp.name}</option>
            `);
            
            if(emp != null){
                $('#ngaykihopdong').val(emp);
            }
            if(para.salary_parameter != null){
                if(para.salary_parameter.don_gia_dich_vu != null){
                    $('#dongiadv').val(para.salary_parameter.don_gia_dich_vu);
                }
                if(para.salary_parameter.he_so_pc_kiem_nhiem != null){
                    $('#hesoquypckiem').val(para.salary_parameter.he_so_pc_kiem_nhiem);
                }
                if(para.salary_parameter.muc_tham_gia_bao_hiem != null){
                    $('#mucthamgiabh').val(para.salary_parameter.muc_tham_gia_bao_hiem);
                }
                if(para.salary_parameter.ngay_cong != null){
                    $('#ngaycong').val(para.salary_parameter.ngay_cong);
                }
                if(para.salary_parameter.phu_cap_uu_dai != null){
                    $('#pcuudai').val(para.salary_parameter.phu_cap_uu_dai);
                }
                if(para.salary_parameter.tien_dien_thoai != null){
                    $('#dienthoai').val(para.salary_parameter.tien_dien_thoai);
                }
                if(para.salary_parameter.an_ca != null){
                    $('#anca').val(para.salary_parameter.an_ca);
                }
                if(para.salary_parameter.xep_loai != null){
                    $('#xeploai option').each(function(){
                        if($(this).attr('value') == para.salary_parameter.xep_loai){
                            $(this).attr('selected','');
                        }
                    });
                }
            }
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
    });
    // tính lương
    $('#tinhluong').on('click', function(e){
        user.account_id = $('#staff_id').val();
        e.preventDefault();
        var dateObject = ($('#ngaykihopdong').val()).split('-');
        dateObject = dateObject[2]+'/'+dateObject[1]+'/'+dateObject[0];
        var mtgbh = ($('#mucthamgiabh').val()).split(',');
        var dongiadv = ($('#dongiadv').val()).split(',');
        var kh='';
        for(let i=0;i<mtgbh.length;i++){
            kh = kh+mtgbh[i]
        }
        var dgdv='';
        for(let i=0;i<dongiadv.length;i++){
            dgdv = dgdv+dongiadv[i]
        }
        var data = {
            "trinh_do" :  (typeof $('#level').val() != '') ? $('#level').val() : null,
            "chuc_vu" : null,
            "chuc_danh" : $('#position').val(),
            "kiem_nhiem": null,
            "ngay_ki_hop_dong" : dateObject,
            "xep_loai" : $('#xeploai').val(),
            "ngay_cong" : $('#ngaycong').val(),
            "quy_mo_sv" : $('#quymosv').val(),
            "kmp_thang" : $('#kmp').val(),
            "so_tiet_quy_doi" : $('#sotietquydoi').val(),
            "an_ca" : parseInt($('#anca').val())+'000',
            "tien_dien_thoai" : parseInt($('#dienthoai').val())+'000',
            "muc_tham_gia_bao_hiem" : kh,
            "don_gia_dich_vu" : dgdv,
            "he_so_hd_KLGD" : $('#KLGD').val(),
            "he_so_quy_mo_sv" : $('#hesoquymosinhvien').val(),
            "he_so_pc_kiem_nhiem" : $('#hesoquypckiem').val(),
            "he_so" : $('#heso').val(),
            "phu_cap_uu_dai" : $('#pcuudai').val(),
        }
        $.ajax({
            url: '{{route("salary.calculator_salary")}}',
            type: 'post',
            data: {
                "_token": "{{ csrf_token() }}",
                ...data
            }
        }).done(resp => {
            user.data = resp;
            $('.priceresp').eq(0).html(resp.khoi_luong_tiet_giang_khoa);
            $('.priceresp').eq(1).html(parseInt(resp.luong_theo_tiet_giang.don_gia_tiet_giang).toLocaleString() +' VND');
            $('.priceresp').eq(2).html(parseInt(resp.luong_theo_tiet_giang.don_gia_phu_cap_tiet_giang).toLocaleString() +' VND');
            $('.priceresp').eq(3).html(parseInt(resp.luong_theo_tiet_giang.thanh_tien).toLocaleString() +' VND');
            $('.priceresp').eq(4).html(parseInt(resp.phu_cap_dung_lop).toLocaleString() +' VND');
            $('.priceresp').eq(5).html(parseInt(resp.luong_ql_phu_cap_khac).toLocaleString() +' VND');
            $('.priceresp').eq(6).html(parseInt(resp.phu_cap_tham_nien).toLocaleString() +' VND');
            $('.priceresp').eq(7).html(parseInt(resp.tong_thu_nhap).toLocaleString() +' VND');
            $('.priceresp').eq(8).html(parseInt(resp.tong_thu_nhap_chiu_thue).toLocaleString() +' VND');
            $('.priceresp').eq(9).html(parseInt(resp.khau_tru_thue_tncn).toLocaleString() +' VND');
            $('.priceresp').eq(10).html(parseInt(resp.bao_hiem.BHXH).toLocaleString() +' VND');
            $('.priceresp').eq(11).html(parseInt(resp.bao_hiem.BHYT).toLocaleString() +' VND');
            $('.priceresp').eq(12).html(parseInt(resp.bao_hiem.BHTN).toLocaleString() +' VND');
            $('.priceresp').eq(13).html(resp.thuc_nhan +' VND');
            $('#kqtv').show();
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
    })
    $('.price').on('change click keyup input paste',(function (event) {
    $(this).val(function (index, value) {
        return value.replace(/(?!\.)\D/g, "").replace(/(?<=\..*)\./g, "").replace(/(?<=\.\d\d).*/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    });
}));

$('#save').on('click', function(e){
    e.preventDefault();
    $.ajax({
            url:'{{route("salary.SaveNoty")}}',
            type: 'post',
            data: {
                ...user,
                "_token": "{{ csrf_token() }}",
            }
        }).done(resp => {
            if(resp.status){
                swal({
                    icon: 'success',
                    title: 'Thành công',
                    text: 'Đã xử lý thành công!',
                })
            }else{
                swal({
                    icon: 'error',
                    title: 'Lỗi',
                    text: 'Lỗi hệ thống!',
                })
            }
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
});
$('#savewithout').on('click', function(e){
    e.preventDefault();
    $.ajax({
            url:'{{route("salary.withoutSaveNoty")}}',
            type: 'post',
            data: {
                ...user,
                "_token": "{{ csrf_token() }}",
            }
        }).done(resp => {
            if(resp.status){
                swal({
                    icon: 'success',
                    title: 'Thành công',
                    text: 'Đã xử lý thành công!',
                })
            }else{
                swal({
                    icon: 'error',
                    title: 'Lỗi',
                    text: 'Lỗi hệ thống!',
                })
            }
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
})
});



</script>
@endsection
