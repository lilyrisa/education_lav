@extends('layouts.master')
@section('page_title', 'Báo cáo lương')
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Báo cáo lương</h6>
            {!! Qs::getPanelOptions() !!}
        </div>

        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane show  active fade" id="new-salary">
                    <div class="row">
                        <div class="col-md-12">
                            <form>
                            <div class="row" id="form">
                                <div class="form-group col-md-3 col-sm-12">
                                    <label for="type_user" class="col-lg-12 col-form-label font-weight-semibold">Loại nhân viên <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="Chọn giảng viên" class="form-control select-search" name="type_user" id="type_user">
                                            <option value="">Chọn nhân viên</option>
                                            <option value="3" selected>Giảng viên</option>
                                            <option value="5">Nhân viên</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 col-sm-12">
                                    <label for="users" class="col-lg-12 col-form-label font-weight-semibold">Danh sách nhân viên <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="Chọn giảng viên" class="form-control select-search" name="users" id="users">
                                            @foreach($lsUser as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-3 col-sm-12">
                                    <label for="position" class="col-lg-12 col-form-label font-weight-semibold">Chọn tháng <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select required data-placeholder="Chọn tháng" class="form-control select-search" name="date_salary" id="date_salary">
                                            @for($i=1; $i<=12; $i++)
                                            <option value="{{$i}}">Tháng {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 col-sm-12">
                                    <label for="position" class="col-lg-12 col-form-label font-weight-semibold">Chọn năm <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" name="year_salary" id="year_salary" value="{{ now()->year }}">
                                    </div>
                                </div>

                                <div class="form-group col-md-12 col-sm-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary" id="getdata">Lấy dữ liệu <i class="icon-paperplane ml-2"></i></button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="card" id="result_salary" style="display:none"> 
        {{-- style="display:none" --}}
        <div class="card-header header-elements-inline">
            <h6 class="card-title">Báo cáo lương</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <table class="table table-condensed table-hover datatable-button-html5-columns" id="data_table">
                    <thead>
                        <tr>
                            <th rowspan="2">Họ và tên</th>
                            <th colspan="3" style="text-align: center">Bảo hiểm</th>
                            <th rowspan="2">Tổng thu nhập</th>
                            <th rowspan="2">Khấu trừ thuế thu nhập cá nhân</th>
                            <th rowspan="2">Thực nhận</th>
                            <th rowspan="2">Tháng / năm</th>
                            
                        </tr>
                        <tr>
                            <th>Bảo hiểm xã hội</th>
                            <th>Bảo hiểm y tê</th>
                            <th>Bảo hiểm thất nghiệp</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--subject List Ends--}}

@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var t = $('#data_table').DataTable();
    // get user_type
    $('#type_user').on('change', function(){
        var id = $(this).val();

        // get list user
        $.ajax({
            url:'{{route("salary.users")}}',
            type: 'post',
            data: {
                user: id,
                "_token": "{{ csrf_token() }}",
            }
        }).done(resp => {
            console.log(resp);
            var tmp = '';
            resp.forEach((v) => {
                tmp += `<option value="${v.id}">${v.name}</option>`;
            });
            $('#users').html(tmp);
            
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
    });
    var month;
    var year
    $('#getdata').on('click', function(e){
        e.preventDefault();
        month = $('#date_salary').val();
        year = $('#year_salary').val();
        $.ajax({
            url:'{{route("salary.report_ajax")}}',
            type: 'post',
            data: {
                type_u: $('#type_user').val(),
                user: $('#users').val(),
                month: month,
                year: year,
                "_token": "{{ csrf_token() }}",
            }
        }).done(resp => {
            console.log(resp);
            if(resp.length > 0){
                $('#result_salary').show();
                resp.forEach((v) => {
                    t.row.add( [
                        v.user.name,
                        parseInt(v.BHXH).toLocaleString() +' VND',
                        parseInt(v.BHYT).toLocaleString() +' VND',
                        parseInt(v.BHTN).toLocaleString() +' VND',
                        parseInt(v.tong_thu_nhap).toLocaleString() +' VND',
                        parseInt(v.khau_tru_thue_tncn).toLocaleString() +' VND',
                        v.thuc_nhan+'VND',
                        month+'/'+year,
                    ] ).draw( false );
                });
            }else{
                swal({
                    icon: 'error',
                    title: 'Lỗi',
                    text: 'Không tìm thấy dữ liệu!',
                })
            }
            
        }).fail((e,st) =>{
            alert( "Request failed: " + st);
        });
    })
});



</script>
@endsection
