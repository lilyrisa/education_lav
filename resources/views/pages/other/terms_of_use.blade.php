@extends('layouts.login_master')

@section('content')

    <div class="page-content">
        <div class="content-wrapper">
            <div class="content">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title font-weight-bold text-center">ĐIỀU KHOẢN SỬ DỤNG</h1>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div style="font-size: 16px;" class="col-md-10 offset-md-1">
                                <p>Sửa đổi lần cuối: ngày 4 tháng 11 năm 2019</p>

                                <h4 class="font-weight-semibold">Chấp nhận các Điều khoản Sử dụng</h4>

                                <p>Các điều khoản sử dụng này được ký kết bởi và giữa Bạn và {{ $app_name }} Các điều khoản và điều kiện sau đây và tất cả các tài liệu được kết hợp bằng cách tham chiếu (gọi chung là "Điều khoản Sử dụng"), chi phối quyền truy cập và sử dụng của bạn <a target="_blank" href="{{ $app_url }}">{{ $app_url }}</a>, bao gồm mọi nội dung, chức năng và dịch vụ được cung cấp trên hoặc thông qua <a target="_blank" href="{{ $app_url }}">{{ $app_url }}</a> ("Website").</p>

                                <p>Vui lòng đọc kỹ Điều khoản Sử dụng trước khi bạn bắt đầu sử dụng Trang web. Bằng cách sử dụng Trang web, bạn chấp nhận và đồng ý bị ràng buộc và tuân theo các Điều khoản Sử dụng này và Chính sách Bảo mật của chúng tôi, được tìm thấy tại <a target="_blank" href="{{ route('privacy_policy') }}">{{ route('privacy_policy') }}</a>, được kết hợp ở đây bằng cách tham khảo. Nếu bạn không muốn đồng ý với các Điều khoản Sử dụng này hoặc Chính sách Bảo mật, bạn không được truy cập hoặc sử dụng Trang web.</p>

                                <h4 class="font-weight-semibold">Changes To The Terms Of Use</h4>

                                <p>Chúng tôi có thể sửa đổi và cập nhật các Điều khoản Sử dụng này theo quyết định riêng của chúng tôi. Tất cả các thay đổi có hiệu lực ngay lập tức khi chúng tôi đăng chúng. Tuy nhiên, bất kỳ thay đổi nào đối với các điều khoản giải quyết tranh chấp được quy định trong Luật điều chỉnh và Quyền tài phán sẽ không áp dụng cho bất kỳ tranh chấp nào mà các bên đã thông báo thực tế trước ngày thay đổi được đăng trên Trang web.</p>

                                <p>Bạn sẽ tiếp tục sử dụng Trang web sau khi đăng ký Điều khoản Sử dụng các sửa đổi có nghĩa là bạn chấp nhận và đồng ý với các thay đổi. Bạn phải kiểm tra trang này theo thời gian để biết về bất kỳ sự thay đổi nào, vì họ ràng buộc bạn.</p>

                                <h4 class="font-weight-semibold">Truy cập trang web và bảo mật tài khoản</h4>
                                <p>Chúng tôi có quyền thu hồi hoặc sửa đổi Trang web này, và bất kỳ dịch vụ hoặc tài liệu nào mà chúng tôi cung cấp trên Trang web, theo quyết định riêng của chúng tôi mà không cần thông báo. Chúng tôi sẽ không chịu trách nhiệm nếu vì bất kỳ lý do gì mà tất cả hoặc bất kỳ phần nào của Trang web không khả dụng vào bất kỳ lúc nào hoặc trong bất kỳ khoảng thời gian nào. Đôi khi, chúng tôi có thể hạn chế quyền truy cập vào một số phần của Trang web hoặc toàn bộ Trang web đối với người dùng.</p>

                                <h3>Bài báo I</h3>
                                <p>Bạn chịu trách nhiệm cho:</p>

                                <ul>
                                    <li>Thực hiện mọi sự sắp xếp cần thiết để bạn có thể truy cập vào Trang web.</li>
                                    <li>Đảm bảo rằng tất cả những người truy cập Trang web thông qua kết nối internet của bạn đều biết các Điều khoản Sử dụng này và tuân thủ chúng.</li>
                                </ul>

                                <p>Để truy cập Trang web hoặc một số tài nguyên mà Trang web cung cấp, bạn có thể được yêu cầu cung cấp một số chi tiết đăng ký hoặc thông tin khác. Điều kiện sử dụng Trang web của bạn là tất cả thông tin bạn cung cấp trên Trang web là chính xác, cập nhật và đầy đủ. Bạn đồng ý rằng tất cả thông tin bạn cung cấp để đăng ký với Trang web này hoặc bằng cách khác, bao gồm nhưng không giới hạn thông qua việc sử dụng bất kỳ tính năng tương tác nào trên Trang web, được điều chỉnh bởi Chính sách bảo mật của chúng tôi và bạn đồng ý với tất cả các hành động chúng tôi thực hiện đối với thông tin phù hợp với Chính sách bảo mật của chúng tôi.</p>

                                <p>Nếu bạn chọn hoặc được cung cấp tên người dùng, mật khẩu hoặc bất kỳ phần thông tin nào khác như một phần của quy trình bảo mật của chúng tôi, bạn phải coi thông tin đó là bí mật và bạn không được tiết lộ nó cho bất kỳ cá nhân hoặc tổ chức nào khác. Bạn cũng thừa nhận rằng tài khoản của bạn là cá nhân đối với bạn và đồng ý không cung cấp cho bất kỳ người nào khác quyền truy cập vào Trang web này hoặc các phần của nó bằng tên người dùng, mật khẩu hoặc thông tin bảo mật khác của bạn. Bạn đồng ý thông báo cho chúng tôi ngay lập tức về bất kỳ truy cập trái phép nào vào hoặc sử dụng tên người dùng hoặc mật khẩu của bạn hoặc bất kỳ hành vi vi phạm bảo mật nào khác. Bạn cũng đồng ý đảm bảo rằng bạn thoát khỏi tài khoản của mình vào cuối mỗi phiên. Bạn nên đặc biệt thận trọng khi truy cập tài khoản của mình từ máy tính công cộng hoặc máy tính dùng chung để người khác không thể xem hoặc ghi lại mật khẩu hoặc thông tin cá nhân khác của bạn.</p>

                                <p>Chúng tôi có quyền vô hiệu hóa bất kỳ tên người dùng, mật khẩu hoặc số nhận dạng khác, cho dù do bạn chọn hoặc do chúng tôi cung cấp, vào bất kỳ lúc nào theo quyết định riêng của chúng tôi vì bất kỳ lý do nào hoặc không, bao gồm nếu, theo quan điểm của chúng tôi, bạn đã vi phạm bất kỳ điều khoản nào của các Điều khoản Sử dụng này.</p>

                                <h4 class="font-weight-semibold">Quyền sở hữu trí tuệ</h4>

                               <p>Trang web và toàn bộ nội dung, tính năng và chức năng của nó (bao gồm nhưng không giới hạn ở tất cả thông tin, phần mềm, văn bản, màn hình, hình ảnh, video và âm thanh cũng như thiết kế, lựa chọn và sắp xếp chúng), thuộc sở hữu của chúng tôi, người cấp phép của chúng tôi, hoặc các nhà cung cấp khác của tài liệu đó và được bảo vệ bởi Hoa Kỳ và quốc tế về bản quyền, nhãn hiệu, bằng sáng chế, bí mật thương mại và các luật sở hữu trí tuệ hoặc quyền sở hữu khác.</p>

                                <p>Các Điều khoản Sử dụng này cho phép bạn sử dụng Trang web chỉ cho mục đích sử dụng cá nhân, phi thương mại. Bạn không được sao chép, phân phối, sửa đổi, tạo ra các tác phẩm phái sinh, hiển thị công khai, trình diễn công khai, xuất bản lại, tải xuống, lưu trữ hoặc truyền tải bất kỳ tài liệu nào trên Trang web của chúng tôi nếu vi phạm bất kỳ luật nào.</p>

                                <p>Bạn không được truy cập hoặc sử dụng cho bất kỳ mục đích thương mại nào bất kỳ phần nào của Trang web hoặc bất kỳ dịch vụ hoặc tài liệu nào có sẵn thông qua Trang web.</p>

                                <h4 class="font-weight-semibold">Nhãn hiệu</h4>

                                <p>Tên {{$app_name}} và tất cả các tên, biểu trưng, khẩu hiệu, phương châm và thiết kế có liên quan là thương hiệu của chúng tôi hoặc các chi nhánh hoặc nhà cấp phép của chúng tôi. Bạn không được sử dụng những nhãn hiệu đó mà không có sự cho phép trước bằng văn bản của chúng tôi. Tất cả các tên, biểu tượng, tên sản phẩm và dịch vụ, thiết kế và khẩu hiệu khác trên Trang web này là thương hiệu của chủ sở hữu tương ứng.</p>

                                <h4 class="font-weight-semibold">Sử dụng bị Cấm</h4>

                                <ul>
                                    <li>Bạn chỉ có thể sử dụng Trang web cho các mục đích hợp pháp và tuân theo các Điều khoản Sử dụng này.</li>
                                    <li>Theo bất kỳ cách nào vi phạm bất kỳ luật hoặc quy định hiện hành nào của liên bang, tiểu bang, địa phương hoặc quốc tế (bao gồm nhưng không giới hạn, bất kỳ luật nào liên quan đến việc xuất dữ liệu hoặc phần mềm đến và đi từ Hoa Kỳ hoặc các quốc gia khác).</li>
                                    <li>Với mục đích lợi dụng, gây hại hoặc cố gắng lợi dụng hoặc gây hại cho trẻ vị thành niên theo bất kỳ cách nào bằng cách cho họ xem nội dung không phù hợp, yêu cầu thông tin nhận dạng cá nhân hoặc các cách khác.</li>
                                    <li>Để gửi, nhận, tải lên, tải xuống, sử dụng hoặc tái sử dụng bất kỳ tài liệu nào không tuân thủ Tiêu chuẩn nội dung được nêu trong Điều khoản sử dụng này.</li>
                                    <li>Truyền tải hoặc mua sắm gửi bất kỳ tài liệu quảng cáo hoặc khuyến mại nào mà không có sự đồng ý trước bằng văn bản của chúng tôi, bao gồm bất kỳ "thư rác", hoặc "spam mail" hoặc bất kỳ lời chào mời tương tự nào khác.</li>
                                    <li>Mạo danh hoặc cố gắng mạo danh chúng tôi, một trong những nhân viên của chúng tôi, một người dùng khác hoặc bất kỳ cá nhân hoặc tổ chức nào khác (bao gồm nhưng không giới hạn, bằng cách sử dụng địa chỉ e-mail hoặc tên hiển thị được liên kết với bất kỳ điều nào ở trên.)</li>
                                    <li>Tham gia vào bất kỳ hành vi nào khác hạn chế hoặc ức chế việc sử dụng hoặc thưởng thức Trang web của bất kỳ ai, hoặc hành vi, theo xác định của chúng tôi, có thể gây hại cho chúng tôi hoặc người dùng Trang web hoặc khiến họ phải chịu trách nhiệm pháp lý.</li>
                                </ul>

                                <h4 class="font-weight-semibold">Ngoài ra, bạn đồng ý không làm:</h4>
                                <ul>
                                    <li>Sử dụng Trang web theo bất kỳ cách nào có thể vô hiệu hóa, quá tải, làm hỏng hoặc làm suy yếu trang web hoặc gây trở ngại cho việc sử dụng Trang web của bất kỳ bên nào khác, bao gồm khả năng họ tham gia vào các hoạt động thời gian thực thông qua Trang web.</li>
                                    <li>Use any robot, spider or other automatic device, process or means to access the Website for any purpose, including monitoring or copying any of the material on the Website.</li>
                                    <li>Use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent.</li>
                                    <li>Use any device, software or routine that interferes with the proper working of the Website.</li>
                                    <li>Introduce any viruses, Trojan horses, worms, logic bombs or other material which is malicious or technologically harmful.</li>
                                    <li>Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or database connected to the Website.</li>
                                    <li>Attack the Website via a denial-of-service attack or a distributed denial-of-service attack.</li>
                                    <li>Use the Website in any manner that violates any applicable {{ $app_name }} policy, rule, or procedure.</li>
                                    <li>Use the Website in any manner that contravenes Catholic Mercy tradition, faith, and morals or the legacy of Catholic Mercy education.</li>
                                    <li>Otherwise attempt to interfere with the proper working of the Website.</li>
                                </ul>

                                <h4 class="font-weight-semibold">User Contributions</h4>

                                <p>The Website may contain message boards, chat rooms, personal web pages or profiles, forums, bulletin boards, and other interactive features (collectively, "Interactive Services") that allow users to post, submit, publish, display or transmit to other users or other persons (hereinafter, "post") content or materials (collectively, "User Contributions") on or through the Website.</p>

                                <p>All User Contributions must comply with the Content Standards set out in these Terms of Use.</p>

                                <p>Any User Contribution you post to the site will be considered non-confidential and non-proprietary. By providing any User Contribution on the Website, you grant us and our licensees, successors and assigns the right to use, reproduce, modify, perform, display, distribute and otherwise disclose to third parties any such material.</p>

                                <p>You represent and warrant that:</p>

                                <ul>
                                    <li>You own or control all rights in and to the User Contributions and have the right to grant the license granted above to us and our licensees, successors and assigns.</li>
                                    <li> All of your User Contributions do and will comply with these Terms of Use.</li>
                                </ul>

                                <p>You understand and acknowledge that you are responsible for any User Contributions you submit or contribute, and you, not us, have full responsibility for such content, including its legality, reliability, accuracy, and appropriateness.</p>

                                <p>We are not responsible, or liable to any third party, for the content or accuracy of any User Contributions posted by you or any other user of the Website.</p>

                                <h4 class="font-weight-semibold">Monitoring And Enforcement; Termination</h4>
                                <p>We have the right to:</p>

                                <ul>
                                    <li>Remove or refuse to post any User Contributions for any or no reason in our sole discretion.</li>
                                    <li>Take any action with respect to any User Contribution that we deem necessary or appropriate in our sole discretion, including if we believe that such User Contribution violates the Terms of Use, including the Content Standards, infringes any intellectual property right or other right of any person or entity, threatens the personal safety of users of the Website or the public or could create liability for us.</li>
                                    <li>Disclose your identity or other information about you to any third party who claims that material posted by you violates their rights, including their intellectual property rights or their right to privacy.</li>
                                    <li>Take appropriate legal action, including without limitation, referral to law enforcement, for any illegal or unauthorized use of the Website.</li>
                                    <li> Terminate or suspend your access to all or part of the Website for any or no reason, including without limitation, any violation of these Terms of Use.</li>
                                </ul>

                               <p> Without limiting the foregoing, we have the right to fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity or other information of anyone posting any materials on or through the Website.</p>

                                <p>YOU WAIVE AND HOLD HARMLESS {{ strtoupper($app_name) }} FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY {{ strtoupper($app_name) }} DURING OR AS A RESULT OF ITS INVESTIGATIONS AND FROM ANY ACTIONS TAKEN AS A CONSEQUENCE OF INVESTIGATIONS BY {{ strtoupper($app_name) }} OR LAW ENFORCEMENT AUTHORITIES.</p>

                                <p>However, we do not undertake to review all material before it is posted on the Website, and cannot ensure prompt removal of objectionable material after it has been posted. Accordingly, we assume no liability for any action or inaction regarding transmissions, communications, or content provided by any user or third party. We have no liability or responsibility to anyone for performance or nonperformance of the activities described in this section.</p>

                                <h4 class="font-weight-semibold">Content Standards</h4>

                                <p>These content standards apply to any and all User Contributions and use of Interactive Services. User Contributions must in their entirety comply with all applicable federal, state, local and international laws and regulations. Without limiting the foregoing, User Contributions must not:</p>

                                <ul>
                                    <li> Contain any material which is defamatory, obscene, indecent, abusive, offensive, harassing, violent, hateful, inflammatory or otherwise objectionable.</li>
                                    <li> Promote sexually explicit or pornographic material, violence, or discrimination based on race, sex, religion, nationality, disability, sexual orientation or age.</li>
                                    <li> Infringe any patent, trademark, trade secret, copyright or other intellectual property or other rights of any other person.</li>
                                    <li>Violate the legal rights (including the rights of publicity and privacy) of others or contain any material that could give rise to any civil or criminal liability under applicable laws or regulations or that otherwise may be in conflict with these Terms of Use and our <a target="_blank" href="{{ route('privacy_policy') }}">Privacy Policy</a>.</li>
                                    <li>Be likely to deceive any person.</li>
                                    <li>Promote any illegal activity, or advocate, promote or assist any unlawful act.</li>
                                    <li>Cause annoyance, inconvenience or needless anxiety or be likely to upset, embarrass, alarm or annoy any other person.</li>
                                    <li>Impersonate any person, or misrepresent your identity or affiliation with any person or organization.</li>
                                    <li>Involve commercial activities or sales, such as contests, sweepstakes and other sales promotions, barter or advertising.</li>
                                    <li>Give the impression that they emanate from or are endorsed by us or any other person or entity, if this is not the case.</li>
                                </ul>

                                <h4 class="font-weight-semibold">Copyright Infringement</h4>
                                <p>If you believe that any User Contributions violate your copyright, please contact us</p>

                                <h5 class="font-weight-semibold">Reliance on Information Posted</h5>

                                <p>The information presented on or through the Website is made available solely for general information purposes. We do not warrant the accuracy, completeness or usefulness of this information. Any reliance you place on such information is strictly at your own risk. We disclaim all liability and responsibility arising from any reliance placed on such materials by you or any other visitor to the Website, or by anyone who may be informed of any of its contents.</p>

                                <p>This Website may include content provided by third parties, including materials provided by other users, bloggers and third-party licensors, syndicators, aggregators and/or reporting services. All statements and/or opinions expressed in these materials, and all articles and responses to questions and other content, other than the content provided by us, are solely the opinions and the responsibility of the person or entity providing those materials. These materials do not necessarily reflect the opinion of us. We are not responsible, or liable to you or any third party, for the content or accuracy of any materials provided by any third parties.</p>

                                <h4 class="font-weight-semibold">Changes To The Website</h4>
                                <p>We may update the content on this Website from time to time, but its content is not necessarily complete or up-to-date. Any of the material on the Website may be out of date at any given time, and we are under no obligation to update such material.</p>

                                <p>Information About You and Your Visits to the Website</p>

                                <p>All information we collect on this Website is subject to our Privacy Policy. By using the Website, you consent to all actions taken by us with respect to your information in compliance with the <a target="_blank" href="{{ route('privacy_policy') }}">Privacy Policy</a>.</p>

                                <h3>Article II</h3>

                                <h4 class="font-weight-semibold">Linking to the Website and Social Media Features</h4>

                                <p>vYou may link to our homepage, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part without our express written consent.</p>

                                <p>You agree to cooperate with us in removing any link we ask you to. We reserve the right to withdraw linking permission without notice.</p>

                                <p>We may disable all or any social media features and any links at any time without notice in our discretion.</p>

                                <h4 class="font-weight-semibold">Links from the Website</h4>

                                <p>If the Website contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links contained in advertisements, including banner advertisements and sponsored links. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. If you decide to access any of the third party websites linked to this Website, you do so entirely at your own risk and subject to the terms and conditions of use for such websites.</p>

                                <h4 class="font-weight-semibold">Disclaimer of Warranties</h4>

                                <p>You understand that we cannot and do not guarantee or warrant that files available for downloading from the internet or the Website will be free of viruses or other destructive code. You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for anti-virus protection and accuracy of data input and output, and for maintaining a means external to our site for any reconstruction of any lost data. WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY WEBSITE LINKED TO IT.</p>

                                <p>YOUR USE OF THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. NEITHER {{ strtoupper($app_name) }} NOR ANY PERSON ASSOCIATED WITH {{ strtoupper($app_name) }} MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF THE WEBSITE. WITHOUT LIMITING THE FOREGOING, NEITHER {{ strtoupper($app_name) }} NOR ANYONE ASSOCIATED WITH {{ strtoupper($app_name) }} REPRESENTS OR WARRANTS THAT THE WEBSITE, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.</p>

                                <p>{{ strtoupper($app_name) }} HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF NON-INFRINGEMENT.</p>

                                <p>THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</p>

                                <h4 class="font-weight-semibold">Limitation on Liability</h4>

                                <p>IN NO EVENT WILL {{ strtoupper($app_name) }}, ITS AFFILIATES OR THEIR LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.</p>

                                <p>THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</p>

                                <h4 class="font-weight-semibold">Indemnification</h4>

                                <p>You agree to defend, indemnify and hold harmless {{ $app_name }}, its affiliates, licensors and service providers, and its and their respective officers, directors, employees, contractors, agents, licensors, suppliers, successors and assigns from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses or fees (including reasonable attorneys' fees) arising out of or relating to your violation of these Terms of Use or your use of the Website, including, but not limited to, your User Contributions, any use of the Website's content, services and products other than as expressly authorized in these Terms of Use or your use of any information obtained from the Website.</p>

                                <h4 class="font-weight-semibold">Governing Law and Jurisdiction</h4>

                                <p>All matters relating to the Website and these Terms of Use and any dispute or claim arising therefrom or related thereto (in each case, including non-contractual disputes or claims), shall be governed by and construed in accordance with the laws of Nigeria without giving effect to any choice or conflict of law provision or rule.</p>

                                <p>Any legal suit, action or proceeding arising out of, or related to, these Terms of Use or the Website shall be instituted exclusively in the federal courts of Nigeria, although we retain the right to bring any suit, action or proceeding against you for breach of these Terms of Use in your country of residence or any other relevant country. You waive any and all objections to the exercise of jurisdiction over you by such courts and to venue in such courts.</p>

                                <h4 class="font-weight-semibold">Arbitration</h4>

                                <p>At {{ $app_name }}’s sole discretion, it may require You to submit any disputes arising from the use of these Terms of Use or the Website, including disputes arising from or concerning their interpretation, violation, invalidity, non-performance, or termination, to final and binding arbitration under the Arbitration and Concilliation Act of Nigeria</p>

                                <h4 class="font-weight-semibold">Limitation on Time to File Claims</h4>

                                <p>ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS OF USE OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.</p>

                                <h4 class="font-weight-semibold">Waiver and Severability</h4>

                                <p>No waiver of by {{ $app_name }} of any term or condition set forth in these Terms of Use shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of {{ $app_name }} to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision.</p>

                                <p>If any provision of these Terms of Use is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effect.</p>

                                <h4 class="font-weight-semibold">Entire Agreement</h4>

                                <p>These Terms of Use and our <a target="_blank" href="{{ route('privacy_policy') }}">Privacy Policy</a> constitute the sole and entire agreement between you and {{ $app_name }} with respect to the Website and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral, with respect to the Website.</p>

                                <h5 class="font-weight-semibold">Your Comments and Concerns</h5>
                                <p>If you do have any comments or concerns regarding but not limited to these Terms of Use. Please contact us.</p>

                                <p>This website is operated by {{ $app_name }}.</p>

                                <p>All other feedback, comments, requests for technical support and other communications relating to the Website should be directed to the School Administrator. Please call {{ $contact_phone }}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
