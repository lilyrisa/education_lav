@extends('layouts.login_master')

@section('content')

<div class="page-content">
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title font-weight-bold text-center">CHÍNH SÁCH BẢO MẬT</h1>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div style="font-size: 16px;" class="col-md-10 offset-md-1">
                            <p>Sửa đổi lần cuối: ngày 10 tháng 6 năm 2021</p>

                            <h4 class="font-weight-semibold">Giới thiệu</h4>

                            <p>{{ $app_name }} ("Chúng tôi") tôn trọng sự riêng tư của bạn và cam kết bảo vệ nó thông qua việc tuân thủ chính sách này của chúng tôi.</p>

                            <p>Chính sách này mô tả các loại thông tin mà chúng tôi có thể thu thập từ bạn hoặc bạn có thể cung cấp khi bạn truy cập trang web
                                <a target="_blank" href="{{ $app_url }}">{{ $app_url }}</a> ("Trang web" của chúng tôi) và các phần mềm của chúng tôi để thu thập, sử dụng, duy trì, bảo vệ và tiết lộ thông tin đó.</p>

                            <p>Chính sách này áp dụng cho thông tin chúng tôi thu thập:</p>

                            <ul>
                                <li>Trên trang web này.</li>
                                <li>Trong email, tin nhắn văn bản và các tin nhắn điện tử khác giữa bạn và Trang web này.</li>
                            </ul>

                           <p> Vui lòng đọc kỹ chính sách này để hiểu các chính sách và thông lệ của chúng tôi liên quan đến thông tin của bạn và cách chúng tôi sẽ xử lý thông tin đó. Nếu bạn không đồng ý với các chính sách và thông lệ của chúng tôi, lựa chọn của bạn là không sử dụng Trang web của chúng tôi. Bằng cách truy cập hoặc sử dụng Trang web này, bạn đồng ý với chính sách bảo mật này. Chính sách này có thể thay đổi theo thời gian (xem Thay đổi đối với Chính sách Bảo mật của Chúng tôi). Việc bạn tiếp tục sử dụng Trang web này sau khi chúng tôi thực hiện các thay đổi được coi là chấp nhận những thay đổi đó, vì vậy vui lòng kiểm tra chính sách định kỳ để biết các bản cập nhật.</p>

                            <h4 class="font-weight-semibold">Children Under the Age of 13</h4>

                           <p> Chúng tôi không thu thập thông tin cá nhân của người dùng dưới 13 tuổi. Nếu bạn dưới 13 tuổi, không cung cấp bất kỳ thông tin nào trên Trang web này hoặc trên hoặc thông qua bất kỳ tính năng nào của trang web mà không có sự đồng ý trước của cha mẹ. Nếu chúng tôi biết rằng chúng tôi đã thu thập hoặc nhận thông tin cá nhân từ một người dưới 13 tuổi mà không có sự xác minh của sự đồng ý của cha mẹ, chúng tôi sẽ xóa thông tin đó. Nếu bạn tin rằng chúng tôi có thể có bất kỳ thông tin nào về hoặc về một người dùng dưới 13 tuổi, vui lòng gọi {{ $contact_phone }}</p>

                            <h4 class="font-weight-semibold">Thông tin chúng tôi thu thập về bạn và cách chúng tôi thu thập thông tin</h4>

                            <p>Chúng tôi thu thập một số loại thông tin từ và về người dùng Trang web của chúng tôi, bao gồm thông tin:</p>

                            <ul>
                                <li>có thể được nhận dạng cá nhân ("thông tin cá nhân");</li>
                                
                                <li>về kết nối internet của bạn, thiết bị bạn sử dụng để truy cập Trang web của chúng tôi và chi tiết sử dụng.</li>
                            </ul>

                            <p>Chúng tôi thu thập thông tin này:</p>

                            <ul>
                                <li>Trực tiếp từ bạn khi bạn cung cấp cho chúng tôi.</li>
                                <li>Tự động khi bạn điều hướng qua trang web. Thông tin được thu thập tự động có thể bao gồm chi tiết sử dụng, địa chỉ IP và thông tin được thu thập thông qua cookie hoặc các công nghệ theo dõi khác.</li>
                            </ul>

                            <h4 class="font-weight-semibold">Thông tin bạn cung cấp cho chúng tôi. Thông tin chúng tôi thu thập trên hoặc thông qua Trang web của chúng tôi có thể bao gồm:</h4>

                            <ul>
                                <li>Thông tin mà bạn cung cấp bằng cách điền vào các biểu mẫu hoặc khảo sát trên Trang web của chúng tôi. Thông tin cá nhân đã gửi sẽ không được chuyển cho bất kỳ bên thứ ba không liên kết nào trừ khi có quy định khác tại thời điểm thu thập. Khi bạn gửi thông tin cá nhân, thông tin đó chỉ được sử dụng cho mục đích đã nêu tại thời điểm thu thập.</li>
                                <li>Hồ sơ và bản sao thư từ của bạn (bao gồm cả địa chỉ email), nếu bạn liên hệ với chúng tôi.</li>
                            </ul>

                            <h4 class="font-weight-semibold">Thông tin chúng tôi thu thập thông qua công nghệ thu thập dữ liệu tự động:</h4>

                            <p>Khi bạn điều hướng và tương tác với Trang web của chúng tôi, chúng tôi có thể sử dụng công nghệ thu thập dữ liệu tự động để thu thập thông tin nhất định về thiết bị, hành động duyệt web và các mẫu của bạn, bao gồm:</p>

                            <ul>
                                <li>Thông tin chi tiết về các lần bạn truy cập vào Trang web của chúng tôi, bao gồm dữ liệu lưu lượng truy cập, dữ liệu vị trí, nhật ký và dữ liệu liên lạc khác và các tài nguyên mà bạn truy cập và sử dụng trên Trang web.</li>
                                <li>Thông tin về máy tính và kết nối internet của bạn, bao gồm địa chỉ IP, hệ điều hành và loại trình duyệt của bạn.</li>
                            </ul>

                            <p>Thông tin chúng tôi thu thập tự động là dữ liệu thống kê và có thể bao gồm thông tin cá nhân, và chúng tôi có thể duy trì nó hoặc liên kết nó với thông tin cá nhân mà chúng tôi thu thập theo những cách khác hoặc nhận từ bên thứ ba. Nó giúp chúng tôi cải thiện Trang web của mình và cung cấp dịch vụ tốt hơn và được cá nhân hóa hơn, bao gồm cả việc cho phép chúng tôi:</p>

                            <ul>
                                <li>Ước tính quy mô đối tượng và các kiểu sử dụng của chúng tôi.</li>
                                <li>Tăng tốc tìm kiếm của bạn.</li>
                                <li>Nhận ra bạn khi bạn quay lại trang web của chúng tôi.</li>
                            </ul>

                            <p>Các công nghệ chúng tôi sử dụng để thu thập dữ liệu tự động này có thể bao gồm:</p>

                            <ul>
                                <li><strong>Cookies</strong> (hoặc cookie của trình duyệt). Cookie là một tệp nhỏ được đặt trên ổ cứng của máy tính của bạn. Bạn có thể từ chối chấp nhận cookie của trình duyệt bằng cách kích hoạt cài đặt thích hợp trên trình duyệt của bạn. Tuy nhiên, nếu bạn chọn cài đặt này, bạn có thể không truy cập được vào một số phần của Trang web của chúng tôi. Trừ khi bạn đã điều chỉnh cài đặt trình duyệt của mình để nó từ chối cookie, hệ thống của chúng tôi sẽ phát hành cookie khi bạn hướng trình duyệt của mình đến Trang web của chúng tôi.</li>
                                <li><strong>Flash Cookies.</strong> Một số tính năng nhất định của Trang web của chúng tôi có thể sử dụng các đối tượng được lưu trữ cục bộ (hoặc cookie Flash) để thu thập và lưu trữ thông tin về sở thích và điều hướng của bạn đến, từ và trên Trang web của chúng tôi. Cookie flash không được quản lý bởi cài đặt trình duyệt giống như được sử dụng cho cookie của trình duyệt. Để biết thông tin về cách quản lý cài đặt quyền riêng tư và bảo mật của bạn cho cookie Flash, hãy xem Lựa chọn về cách chúng tôi sử dụng và tiết lộ thông tin của bạn.</li>
                                <li><strong>Web Beacons.</strong> Các trang trên Trang web của chúng tôi có thể chứa các tệp điện tử nhỏ được gọi là báo hiệu web (còn được gọi là gif rõ ràng, thẻ pixel và gif pixel đơn) cho phép chúng tôi, chẳng hạn, cho phép chúng tôi tính những người dùng đã truy cập các trang đó và trang web liên quan khác thống kê (ví dụ: ghi lại mức độ phổ biến của nội dung trang web nhất định và xác minh tính toàn vẹn của hệ thống và máy chủ).</li>
                            </ul>

                            <h4 class="font-weight-semibold">Cách chúng tôi sử dụng thông tin của bạn</h4>

                            <p>Chúng tôi sử dụng thông tin mà chúng tôi thu thập về bạn hoặc bạn cung cấp cho chúng tôi, bao gồm bất kỳ thông tin cá nhân nào:</p>

                            <ul>
                                <li>Để giới thiệu Trang web của chúng tôi và nội dung của nó cho bạn.</li>
                                <li>Để cho phép bạn tham gia vào các tính năng tương tác trên Trang web của chúng tôi.</li>
                                <li>Theo bất kỳ cách nào khác, chúng tôi có thể mô tả khi bạn cung cấp thông tin.</li>
                                <li>Cho bất kỳ mục đích nào khác với sự đồng ý của bạn.</li>
                            </ul>

                            <h4 class="font-weight-semibold">Tiết lộ thông tin của bạn</h4>

                            <p>Chúng tôi có thể tiết lộ thông tin tổng hợp về người dùng của mình và thông tin không xác định bất kỳ cá nhân nào, không hạn chế.</p>

                            <p>Chúng tôi có thể tiết lộ thông tin cá nhân mà chúng tôi thu thập hoặc bạn cung cấp như được mô tả trong chính sách bảo mật này:</p>

                            <ul>
                                <li>Để thực hiện mục đích mà bạn cung cấp nó.</li>
                                <li>Đối với bất kỳ mục đích nào khác do chúng tôi tiết lộ khi bạn cung cấp thông tin.</li>
                                <li>Với sự đồng ý của bạn.</li>
                            </ul>

                            <p>Chúng tôi cũng có thể tiết lộ thông tin cá nhân của bạn:</p>

                            <ul>
                                <li>Để tuân thủ bất kỳ lệnh tòa, luật hoặc quy trình pháp lý nào, bao gồm cả việc đáp ứng bất kỳ yêu cầu nào của chính phủ hoặc cơ quan quản lý.</li>
                                <li>Để thực thi hoặc áp dụng <a target="_blank" href="{{ route('terms_of_use') }}">Điều khoản sử dụng</a>.</li>
                                <li>Nếu chúng tôi tin rằng việc tiết lộ là cần thiết hoặc thích hợp để bảo vệ quyền, tài sản của chúng tôi hoặc sự an toàn của học sinh hoặc những người khác của chúng tôi.</li>
                            </ul>

                            <h4 class="font-weight-semibold">Lựa chọn về cách chúng tôi sử dụng và tiết lộ thông tin của bạn</h4>
                            <p>Chúng tôi cố gắng cung cấp cho bạn các lựa chọn liên quan đến thông tin cá nhân mà bạn cung cấp cho chúng tôi. Chúng tôi đã tạo các cơ chế để cung cấp cho bạn quyền kiểm soát sau đây đối với thông tin của bạn:</p>

                            <ul>
                                <li><strong>Theo dõi Công nghệ và Quảng cáo </strong>. Bạn có thể đặt trình duyệt của mình để từ chối tất cả hoặc một số cookie của trình duyệt hoặc để cảnh báo bạn khi cookie được gửi. Để tìm hiểu cách bạn có thể quản lý cài đặt cookie Flash của mình, hãy truy cập trang cài đặt trình phát Flash trên trang web của Adobe. Nếu bạn vô hiệu hóa hoặc từ chối cookie, xin lưu ý rằng một số phần của trang web này sau đó có thể không truy cập được hoặc không hoạt động bình thường.</li>
                            </ul>

                            <h4 class="font-weight-semibold">Bảo mật dữ liệu</h4>

                            <p>Chúng tôi đã thực hiện các biện pháp được thiết kế để bảo mật thông tin cá nhân của bạn khỏi bị mất ngẫu nhiên và khỏi bị truy cập, sử dụng, thay đổi và tiết lộ trái phép.</p>

                            <h4 class="font-weight-semibold">Các thay đổi đối với Chính sách quyền riêng tư</h4>

                           <p>Chính sách của chúng tôi là đăng bất kỳ thay đổi nào mà chúng tôi thực hiện đối với chính sách bảo mật của mình trên trang này. Nếu chúng tôi thực hiện những thay đổi quan trọng đối với cách chúng tôi xử lý thông tin cá nhân của người dùng, chúng tôi sẽ thông báo cho bạn thông qua một thông báo trên trang chủ của Trang web. Ngày chính sách bảo mật được sửa đổi lần cuối được xác định ở đầu trang. Bạn có trách nhiệm truy cập định kỳ Trang web của chúng tôi và chính sách bảo mật này để kiểm tra bất kỳ thay đổi nào.</p>

                            <h4 class="font-weight-semibold">Contact Information</h4>

                            <p>Để đặt câu hỏi hoặc nhận xét về chính sách bảo mật này và các thực tiễn về quyền riêng tư của chúng tôi, vui lòng gọi {{ $contact_phone }}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
</div>
</div>
@endsection
