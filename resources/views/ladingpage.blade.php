<!DOCTYPE html>
<html lang="en">
<head>

     <title>Known - Education HTML Template</title>
<!-- 

Known Template 

https://templatemo.com/tm-516-known

-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="{{asset('lading_page/css/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{asset('lading_page/css/font-awesome.min.css')}}">
     <link rel="stylesheet" href="{{asset('lading_page/css/owl.carousel.css')}}">
     <link rel="stylesheet" href="{{asset('lading_page/css/owl.theme.default.min.css')}}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('global_assets/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('global_assets/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('global_assets/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('global_assets/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('global_assets/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('global_assets/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('global_assets/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('global_assets/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('global_assets/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('global_assets/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('global_assets/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('global_assets/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('global_assets/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('global_assets/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('global_assets/images/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="{{asset('lading_page/css/templatemo-style.css')}}">

</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="{{route('home_index')}}" class="navbar-brand">MT ACADEMY</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="#top" class="smoothScroll">Trang chủ</a></li>
                         <li><a href="#about" class="smoothScroll">Thông tin</a></li>
                         <li><a href="#team" class="smoothScroll">Giảng viên</a></li>
                         <li><a href="#courses" class="smoothScroll">Đội ngũ hỗ trợ</a></li>
                         <li><a href="#testimonial" class="smoothScroll">Reviews</a></li>
                         <li><a href="#contact" class="smoothScroll">Liên hệ</a></li>
                         <li><a href="{{route('home')}}" class="smoothScroll">CMS PANEL</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="tel:0342682117"><i class="fa fa-phone"></i> +84 0342 68 2117</a></li>
                    </ul>
               </div>

          </div>
     </section>


     <!-- HOME -->
     <section id="home">
          <div class="row">

                    <div class="owl-carousel owl-theme home-slider">
                         <div class="item item-first">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             <h1>Quản lý học sinh</h1>
                                             <h3>MT Academy giúp nhà trường quản lý hồ sơ học tập (điểm thi, tổng kết điểm, kết quả học tập), quản lý về rèn luyện (điểm danh, khen thưởng, kỷ luật,…). Từ đó tiến hành đánh giá xếp loại học tập, hạnh kiểm hay danh hiệu thi đua</h3>
                                             <a href="{{route('home')}}" class="section-btn btn btn-default smoothScroll">Tham gia ngay</a>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-second">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             <h1>Quản lý Cán bộ</h1>
                                             <h3>Phần mềm quản lý hồ sơ giáo viên, phân công giảng dạy, quản lý các sáng kiến, đề tài, kinh nghiệm cũng như về việc khen thưởng, kỷ luật</h3>
                                             <a href="{{route('home')}}" class="section-btn btn btn-default smoothScroll">Tham gia ngay</a>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-third">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             <h1>Quản lý hệ thống gồm các chức năng</h1>
                                             <h3>Thống kê, báo cáo, phân loại các nhân viên, giảng viên, sinh viên và phụ huynh của nhà trường</h3>
                                             <a href="{{route('home')}}" class="section-btn btn btn-default smoothScroll">Tham gia ngay</a>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
          </div>
     </section>


     <!-- FEATURE -->
     <section id="feature">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-4">
                         <div class="feature-thumb">
                              <span>01</span>
                              <h3>Quản lý học sinh</h3>
                              <p>MT Academy giúp nhà trường quản lý hồ sơ học tập (điểm thi, tổng kết điểm, kết quả học tập),...</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="feature-thumb">
                              <span>02</span>
                              <h3>Quản lý Cán bộ</h3>
                              <p>Phần mềm quản lý hồ sơ giáo viên, phân công giảng dạy, quản lý các sáng kiến, đề tài, kinh nghiệm cũng như về việc khen thưởng, kỷ luật</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="feature-thumb">
                              <span>03</span>
                              <h3>Quản lý hệ thống gồm các chức năng</h3>
                              <p>Thống kê, báo cáo, phân loại các nhân viên, giảng viên, sinh viên và phụ huynh của nhà trường</p>
                         </div>
                    </div>

               </div>
          </div>
     </section>


     <!-- ABOUT -->
     <section id="about">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="about-info">
                              <h2>Bắt đầu hành trình của bạn đến một cuộc sống tốt đẹp hơn</h2>

                              <figure>
                                   <span><i class="fa fa-users"></i></span>
                                   <figcaption>
                                        <h3>Tiện dụng và dễ dàng</h3>
                                        <p>CHúng tôi luôn nỗ lực hoàn thiện giúp cho cuộc sống dễ dàng nhất</p>
                                   </figcaption>
                              </figure>

                              <figure>
                                   <span><i class="fa fa-certificate"></i></span>
                                   <figcaption>
                                        <h3>Miễn phí sử dụng</h3>
                                        <p>Miễn phí là một câu chuyện thật thú vị mà không bao giờ kết thúc</p>
                                   </figcaption>
                              </figure>

                              <figure>
                                   <span><i class="fa fa-bar-chart-o"></i></span>
                                   <figcaption>
                                        <h3>Thống kê chuyên nghiệp</h3>
                                        <p>Việc ngồi làm thống kê không còn kinh khủng như trước</p>
                                   </figcaption>
                              </figure>
                         </div>
                    </div>

                    <div class="col-md-offset-1 col-md-4 col-sm-12">
                         <div class="entry-form">
                              <form action="{{route('home')}}" method="get">
                                   <h2>Bắt đầu ngay hôm nay</h2>

                                   <button class="submit-btn form-control" id="form-submit">Started</button>
                              </form>
                         </div>
                    </div>

               </div>
          </div>
     </section>


     <!-- TEAM -->
     <section id="team">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="section-title">
                              <h2>Đội ngũ làm việc <small>Gặp gỡ và đào tạo từ xa</small></h2>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                         <div class="team-thumb">
                              <div class="team-image">
                                   <img src="{{asset('lading_page/images/author-image1.jpg')}}" class="img-responsive" alt="">
                              </div>
                              <div class="team-info">
                                   <h3>Mark Wilson</h3>
                                   <span>Lý tưởng sống của anh ấy là ở gần một lũ con mèo</span>
                              </div>
                              <ul class="social-icon">
                                   <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                   <li><a href="#" class="fa fa-twitter"></a></li>
                                   <li><a href="#" class="fa fa-instagram"></a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                         <div class="team-thumb">
                              <div class="team-image">
                                   <img src="{{asset('lading_page/images/author-image2.jpg')}}" class="img-responsive" alt="">
                              </div>
                              <div class="team-info">
                                   <h3>Catherine</h3>
                                   <span>Đừng bao giờ rủ cô ấy đi uống rượu!</span>
                              </div>
                              <ul class="social-icon">
                                   <li><a href="#" class="fa fa-google"></a></li>
                                   <li><a href="#" class="fa fa-instagram"></a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                         <div class="team-thumb">
                              <div class="team-image">
                                   <img src="{{asset('lading_page/images/author-image3.jpg')}}" class="img-responsive" alt="">
                              </div>
                              <div class="team-info">
                                   <h3>Jessie Ca</h3>
                                   <span>Cô nàng nghiêm túc thật sự</span>
                              </div>
                              <ul class="social-icon">
                                   <li><a href="#" class="fa fa-twitter"></a></li>
                                   <li><a href="#" class="fa fa-envelope-o"></a></li>
                                   <li><a href="#" class="fa fa-linkedin"></a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                         <div class="team-thumb">
                              <div class="team-image">
                                   <img src="{{asset('lading_page/images/author-image4.jpg')}}" class="img-responsive" alt="">
                              </div>
                              <div class="team-info">
                                   <h3>Andrew Berti</h3>
                                   <span>Có thể bỏ qua tất cả mọi việc trừ việc làm người khác cười</span>
                              </div>
                              <ul class="social-icon">
                                   <li><a href="#" class="fa fa-twitter"></a></li>
                                   <li><a href="#" class="fa fa-google"></a></li>
                                   <li><a href="#" class="fa fa-behance"></a></li>
                              </ul>
                         </div>
                    </div>

               </div>
          </div>
     </section>


     <!-- Courses -->
     <section id="courses">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="section-title">
                              <h2>Hỗ trợ <small>Gặp gỡ chuyên gia và nhận những hỗ trợ</small></h2>
                         </div>

                         <div class="owl-carousel owl-theme owl-courses">
                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="courses-thumb">
                                             <div class="courses-top">
                                                  <div class="courses-image">
                                                       <img src="{{asset('lading_page/images/courses-image1.jpg')}}" class="img-responsive" alt="">
                                                  </div>
                                                  <div class="courses-date">
                                                       <span><i class="fa fa-calendar"></i> 12 / 7 / 2021</span>
                                                       <span><i class="fa fa-clock-o"></i> 7 giờ</span>
                                                  </div>
                                             </div>

                                             <div class="courses-detail">
                                                  <h3><a href="#">Hướng dẫn cách quản lý tổ chức</a></h3>
                                                  <p>Cách nhân nhất để làm quen với hệ thống và cách sử dụng hiệu quả hệ thống</p>
                                             </div>

                                             <div class="courses-info">
                                                  <div class="courses-author">
                                                       <img src="{{asset('lading_page/images/author-image1.jpg')}}" class="img-responsive" alt="">
                                                       <span>Mark Wilson</span>
                                                  </div>
                                                  <div class="courses-price">
                                                       <a href="#"><span>450.000 VND</span></a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="courses-thumb">
                                             <div class="courses-top">
                                                  <div class="courses-image">
                                                       <img src="{{asset('lading_page/images/courses-image2.jpg')}}" class="img-responsive" alt="">
                                                  </div>
                                                  <div class="courses-date">
                                                       <span><i class="fa fa-calendar"></i> 20 / 7 / 2021</span>
                                                       <span><i class="fa fa-clock-o"></i> 4.5 giờ</span>
                                                  </div>
                                             </div>

                                             <div class="courses-detail">
                                                  <h3><a href="#">Hướng dẫn tính năng khác</a></h3>
                                                  <p>Hướng dẫn nâng cấp hệ thống</p>
                                             </div>

                                             <div class="courses-info">
                                                  <div class="courses-author">
                                                       <img src="{{asset('lading_page/images/author-image2.jpg')}}" class="img-responsive" alt="">
                                                       <span>Jessica</span>
                                                  </div>
                                                  <div class="courses-price">
                                                       <a href="#"><span>932.000 VND</span></a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="courses-thumb">
                                             <div class="courses-top">
                                                  <div class="courses-image">
                                                       <img src="{{asset('lading_page/images/courses-image3.jpg')}}" class="img-responsive" alt="">
                                                  </div>
                                                  <div class="courses-date">
                                                       <span><i class="fa fa-calendar"></i> 15 / 8 / 2021</span>
                                                       <span><i class="fa fa-clock-o"></i> 6 giờ</span>
                                                  </div>
                                             </div>

                                             <div class="courses-detail">
                                                  <h3><a href="#">Quản lý nhân viên</a></h3>
                                                  <p>Thao tác với nhân viên của nhà trường</p>
                                             </div>

                                             <div class="courses-info">
                                                  <div class="courses-author">
                                                       <img src="{{asset('lading_page/images/author-image3.jpg')}}" class="img-responsive" alt="">
                                                       <span>Catherine</span>
                                                  </div>
                                                  <div class="courses-price free">
                                                       <a href="#"><span>Free</span></a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="courses-thumb">
                                             <div class="courses-top">
                                                  <div class="courses-image">
                                                       <img src="{{asset('lading_page/images/courses-image4.jpg')}}" class="img-responsive" alt="">
                                                  </div>
                                                  <div class="courses-date">
                                                       <span><i class="fa fa-calendar"></i> 10 / 8 / 2021</span>
                                                       <span><i class="fa fa-clock-o"></i> 8 Hours</span>
                                                  </div>
                                             </div>

                                             <div class="courses-detail">
                                                  <h3><a href="#">Quản lý học sinh và phụ huynh</a></h3>
                                                  <p>Hướng dẫn cách quản lý học sinh và phụ huynh</p>
                                             </div>

                                             <div class="courses-info">
                                                  <div class="courses-author">
                                                       <img src="{{asset('lading_page/images/author-image1.jpg')}}" class="img-responsive" alt="">
                                                       <span>Mark Wilson</span>
                                                  </div>
                                                  <div class="courses-price">
                                                       <a href="#"><span>Free</span></a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="courses-thumb">
                                             <div class="courses-top">
                                                  <div class="courses-image">
                                                       <img src="{{asset('lading_page/images/courses-image5.jpg')}}" class="img-responsive" alt="">
                                                  </div>
                                                  <div class="courses-date">
                                                       <span><i class="fa fa-calendar"></i> 5 / 10 / 2021</span>
                                                       <span><i class="fa fa-clock-o"></i> 10 Hours</span>
                                                  </div>
                                             </div>

                                             <div class="courses-detail">
                                                  <h3><a href="#">Nghiệp vụ &amp; thông số hệ thống</a></h3>
                                                  <p>Hướng dẫn cách setup hệ thống</p>
                                             </div>

                                             <div class="courses-info">
                                                  <div class="courses-author">
                                                       <img src="{{asset('lading_page/images/author-image2.jpg')}}" class="img-responsive" alt="">
                                                       <span>Jessica</span>
                                                  </div>
                                                  <div class="courses-price free">
                                                       <a href="#"><span>Free</span></a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>

                         </div>

               </div>
          </div>
     </section>


     <!-- TESTIMONIAL -->
     <section id="testimonial">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="section-title">
                              <h2>Khách hàng review <small>Từ khắp mọi nơi trên thế giới</small></h2>
                         </div>

                         <div class="owl-carousel owl-theme owl-client">
                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="tst-image">
                                             <img src="{{asset('lading_page/images/tst-image1.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="tst-author">
                                             <h4>Jackson</h4>
                                             <span>Teacher</span>
                                        </div>
                                        <p>Hệ thống hỗ trợ quản lý thật tuyệt vời. Tôi muốn giới thiệu nó cho mọi người!</p>
                                        <div class="tst-rating">
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="tst-image">
                                             <img src="{{asset('lading_page/images/tst-image2.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="tst-author">
                                             <h4>Camila</h4>
                                             <span>accountant</span>
                                        </div>
                                        <p>Tôi thử sử dụng và thật ngạc nhiên về chất lượng đem lại cho tôi</p>
                                        <div class="tst-rating">
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                        </div>
                                   </div>
                              </div>

                              <div class="col-md-4 col-sm-4">
                                   <div class="item">
                                        <div class="tst-image">
                                             <img src="{{asset('lading_page/images/tst-image3.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="tst-author">
                                             <h4>Barbie</h4>
                                             <span>School management</span>
                                        </div>
                                        <p>Thật dễ dàng để sử dụng nó</p>
                                        <div class="tst-rating">
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                             <i class="fa fa-star"></i>
                                        </div>
                                   </div>
                              </div>

                         </div>

               </div>
          </div>
     </section> 


     <!-- CONTACT -->
     <section id="contact">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <form id="contact-form" role="form" action="" method="post">
                              <div class="section-title">
                                   <h2>Liên hệ với chúng tôi <small>Chúng tôi luôn lắng nghe đóng góp của mọi người!</small></h2>
                              </div>

                              <div class="col-md-12 col-sm-12">
                                   <input type="text" class="form-control" placeholder="Họ và tên" name="name" required="">
                    
                                   <input type="email" class="form-control" placeholder="Địa chỉ email" name="email" required="">

                                   <textarea class="form-control" rows="6" placeholder="Thông tin yêu cầu" name="message" required=""></textarea>
                              </div>

                              <div class="col-md-4 col-sm-12">
                                   <input type="submit" class="form-control" name="send message" value="Gửi tin nhắn">
                              </div>

                         </form>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="contact-image">
                              <img src="{{asset('lading_page/images/contact-image.jpg')}}" class="img-responsive" alt="Smiling Two Girls">
                         </div>
                    </div>

               </div>
          </div>
     </section>       


     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Địa chỉ</h2>
                              </div>
                              <address>
                                   <p>304 đường Mỹ Đình<br> Nam Từ Liêm - Hà Nội - 100000</p>
                              </address>

                              <ul class="social-icon">
                                   <li><a href="https://www.facebook.com/dark.knight.os" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                   <li><a href="https://www.instagram.com/__sjager__/" class="fa fa-instagram"></a></li>
                              </ul>

                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 MT Academy</p>
                                   
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Thông tin liên hệ</h2>
                              </div>
                              <address>
                                   <p>+84 0342 68 2117</p>
                                   <p><a href="mailto:bvminh101299@gmail.com">bvminh101299@gmail.com</a></p>
                              </address>

                              <div class="footer_menu">
                                   <h2>Thông tin khác</h2>
                                   <ul>
                                        <li><a href="{{route('privacy_policy')}}">Chính sách bảo mật</a></li>
                                        <li><a href="{{route('terms_of_use')}}">Điều khoản sử dụng</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Nhận thông tin</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Email của bạn" name="email" id="email" required="">
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Nhận thông tin">
                                        </form>
                                        <span><sup>*</sup>chúng tôi không thu thập email của bạn.</span>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>


     <!-- SCRIPTS -->
     <script src="{{asset('lading_page/js/jquery.js')}}"></script>
     <script src="{{asset('lading_page/js/bootstrap.min.js')}}"></script>
     <script src="{{asset('lading_page/js/owl.carousel.min.js')}}"></script>
     <script src="{{asset('lading_page/js/smoothscroll.js')}}"></script>
     <script src="{{asset('lading_page/js/custom.js')}}"></script>

</body>
</html>