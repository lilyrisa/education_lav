@extends('layouts.login_master')

@section('head')
<style>
@media (max-width: 576px) {
    #my_camera video {
        max-width: 80%;
        max-height: 80%;
    }

    #results img {
        max-width: 80%;
        max-height: 80%;

    }
}
@media (min-width: 576px){
    .login-form {
        width: 24rem;
    }
}
</style>
@endsection

@section('content')
    <div class="page-content login-cover">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login card -->
                <form class="login-form ">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <img src="{{asset('global_assets/images/logo.png')}}" id="icon" alt="User Icon" width="40%"/>
                                <h5 class="mb-0">Đăng nhập tài khoản bằng nhận dạng khuôn mặt</h5>
                            </div>



                            <div class="form-group ">
                                <div class="col col-md-12" id="my_camera" >
                            </div>

                            <div class="form-group ">
                                <div class="col col-md-6" id="results">

                            </div>

                            <div class="form-group">
                                <button id="takephoto" type="submit" class="btn btn-primary">Đăng nhập <i class="icon-camera ml-2"></i></button>
                            </div>
                            <div class="form-group">
                                <button id="login_ok" type="submit" class="btn btn-primary btn-block">Đăng nhập <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                        </div>
                    </div>
                </form>

            </div>


        </div>

    </div>
    @endsection

    @section('script')
    <script>
        $(document).ready(function(){
            $('#takephoto').on('click', function(e){
            e.preventDefault();
            Webcam.snap( function(data_uri) {
              $('#results').html(`<img class="img-responsive" id="avatar_lo" src="${data_uri}" />`)
            })
          });
          Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
          });
          Webcam.attach( '#my_camera' );

        $('#login_ok').on('click', function(e){
            e.preventDefault();
            if (typeof $('#avatar_lo').attr('src') === 'undefined'){
                swal({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Hãy chụp ảnh trước!',
                    footer: '<a href="">Why do I have this issue?</a>'
                })
            }else{
              $.ajax({
                url: '{{route("loginpostface")}}',
                type: 'post',
                data:{
                    "_token": "{{ csrf_token() }}",
                    avatar: $('#avatar_lo').attr('src'),
                }
            })
            .done(res => {
              if(res.is){
                // toastr.success('Đăng nhập thành công');
                swal({
                    title: "Nhận diện thành công",
                    text: "Họ và tên: "+res.messenge.name+"\nUsername: "+res.messenge.username+"\nEmail: "+res.messenge.email,
                    icon: "success",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                  setTimeout(()=>{location.reload();},1000);
                })
                
              }else{
                swal({
                    title: "opps...!",
                    text: "Lỗi hệ thống",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                })
              }
            })
            }
              
          });
        });
    </script>
    @endsection
