<?php
return
[
    'denied' => 'TRUY CẬP BỊ TỪ CHỐI ĐỐI VỚI HÀNH ĐỘNG KHÔNG HỢP LỆ',
    'user_invalid' => 'Cả Tên người dùng và Email không được để trống',
    'update_ok' => 'Đã cập nhật bản ghi thành công',
    'store_ok' => 'Bản ghi được tạo thành công',
    'del_ok' => 'Bản ghi đã được xóa thành công',
    'delete_ok' => 'Bản ghi đã được xóa thành công',
    'pu_reset' => 'Đặt lại mật khẩu thành công cho người dùng',
    'p_reset' => 'Đặt lại mật khẩu thành công',
    'p_reset_fail' => 'Bạn đã nhập sai mật khẩu!',
    'lock_exam' => 'Ghim sẽ được yêu cầu để xem kết quả nếu Khóa kiểm tra được đặt',
    'rnf' => 'Không tìm thấy bản ghi',
    'srnf' => 'Không tìm thấy hồ sơ học sinh',
    'ernf' => 'Không tìm thấy hồ sơ bài kiểm tra',
    'nstp' => 'KHÔNG có sinh viên nào để nhận học bổng',
    'pin_max' => 'Đã vượt quá giới hạn tạo mã pin (500)',
    'pin_fail' => 'Ghim này đã được người dùng khác sử dụng hoặc Đã vượt quá Giới hạn sử dụng là (5)',
    'pin_create' => 'Đã tạo thành công các ghim',
    'del_teacher' => 'Trước khi bạn có thể xóa Giáo viên này, Bạn phải tìm một người thay thế cho (các) môn học mà Người đó dạy. Xem Hồ sơ Giáo viên để xem Các môn học của Anh / Cô ấy',
    'invalid_time_slot' => 'Thời gian bắt đầu và thời gian kết thúc không thể giống nhau',

];