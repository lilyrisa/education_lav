<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_stores', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_id_salary')->unsigned();
            $table->foreign('user_id_salary')->references('id')->on('users')->onDelete('cascade');
            $table->integer('khoi_luong_tiet_giang_khoa');
            $table->integer('don_gia_tiet_giang');
            $table->integer('don_gia_phu_cap_tiet_giang');
            $table->integer('thanh_tien');
            $table->integer('phu_cap_dung_lop');
            $table->integer('luong_ql_phu_cap_khac');
            $table->integer('phu_cap_tham_nien');
            $table->integer('tong_thu_nhap');
            $table->integer('tong_thu_nhap_chiu_thue');
            $table->integer('khau_tru_thue_tncn');
            $table->integer('BHXH');
            $table->integer('BHYT');
            $table->integer('BHTN');
            $table->string('thuc_nhan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_stores');
    }
}
