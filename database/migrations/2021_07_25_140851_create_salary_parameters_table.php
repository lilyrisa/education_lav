<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_parameters', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_id_para')->unsigned();
            $table->foreign('user_id_para')->references('id')->on('users')->onDelete('cascade');
            $table->string('xep_loai')->nullable();
            $table->integer('ngay_cong')->nullable();
            $table->integer('don_gia_dich_vu')->nullable();
            $table->integer('an_ca')->nullable();
            $table->integer('tien_dien_thoai')->nullable();
            $table->double('he_so_pc_kiem_nhiem')->nullable();
            $table->double('phu_cap_uu_dai')->nullable();
            $table->integer('muc_tham_gia_bao_hiem')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_parameters');
    }
}
