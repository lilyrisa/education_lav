<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryStoreAccontantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_store_accontants', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_id_salary')->unsigned();
            $table->foreign('user_id_salary')->references('id')->on('users')->onDelete('cascade');
            $table->integer('luong_tham_nien');
            $table->integer('he_so_ht');
            $table->integer('thanh_tien');
            $table->integer('luong_le_phep');
            $table->integer('tong_thu_nhap');
            $table->integer('tong_thu_nhap_chiu_thue');
            $table->integer('khau_tru_bao_hiem');
            $table->integer('khau_tru_thue_tncn');
            $table->integer('BHXH');
            $table->integer('BHYT');
            $table->integer('BHTN');
            $table->string('thuc_nhan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_store_accontants');
    }
}
