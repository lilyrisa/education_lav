<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaceRecognsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('face_recogns', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_face_id')->unsigned();
            $table->foreign('user_face_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('id_cloud');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('face_recogns');
    }
}
