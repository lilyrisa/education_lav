<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('class_types')->delete();

        $data = [
            ['name' => 'Công nghệ thông tin', 'code' => 'CNTT'],
            ['name' => 'Kĩ thuật ô tô', 'code' => 'KTOO'],
            ['name' => 'Ngôn ngữ anh', 'code' => 'NNA'],
            ['name' => 'Quản trị khách sạn', 'code' => 'QTKS'],
            ['name' => 'Quản trị kinh doanh', 'code' => 'QTKD'],
            ['name' => 'Sư phạm nghệ thuật', 'code' => 'SPNT'],
        ];

        DB::table('class_types')->insert($data);

    }
}
