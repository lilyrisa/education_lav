<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DormsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dorms')->delete();
        $data = [
            ['name' => 'Kí túc xá 1'],
            ['name' => 'Kí túc xá 2'],
            ['name' => 'Kí túc xá 3'],
            ['name' => 'Khách sạn vip'],
            ['name' => 'Khách sạn vip 1'],
        ];
        DB::table('dorms')->insert($data);
    }
}
