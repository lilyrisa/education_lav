<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'ketoan', 'name' => 'Kế toán', 'level' => 5],
            ['title' => 'canbocapcao', 'name' => 'Cán bộ cấp cao', 'level' => 5],
            ['title' => 'canbocapcao_1', 'name' => 'Cán bộ cấp cao 1', 'level' => 5],
            ['title' => 'canbocapcao_2', 'name' => 'Cán bộ cấp cao 2', 'level' => 5],
            ['title' => 'chuyenvien', 'name' => 'Chuyên viên', 'level' => 5],
            ['title' => 'nhanvien', 'name' => 'Nhân viên', 'level' => 5],
            ['title' => 'nhanvien_1', 'name' => 'Nhân viên 1', 'level' => 5],
            ['title' => 'phuhuynh', 'name' => 'Phụ huynh', 'level' => 4],
            ['title' => 'gianvien', 'name' => 'Giảng viên', 'level' => 3],    
            ['title' => 'giangvien_1', 'name' => 'Giảng viên 1', 'level' => 3],
            ['title' => 'giangvien_2', 'name' => 'Giảng viên 2', 'level' => 3],
            ['title' => 'giangviencapcao', 'name' => 'Giảng viên cao cấp', 'level' => 3],
            ['title' => 'admin', 'name' => 'Admin', 'level' => 2],
            ['title' => 'super_admin', 'name' => 'Super Admin', 'level' => 1],
        ];
        DB::table('user_types')->insert($data);
    }
}
