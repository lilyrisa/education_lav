<?php
namespace Database\Seeders;

use App\Models\ClassType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class MyClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('my_classes')->delete();
        $ct = ClassType::pluck('id')->all();

        $data = [
            ['name' => 'D101-k9', 'class_type_id' => $ct[0]],
            ['name' => 'D101-k10', 'class_type_id' => $ct[0]],
            ];

        DB::table('my_classes')->insert($data);

    }
}
