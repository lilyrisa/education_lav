<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function(){

    Route::post('/luong_cong_viec', 'CalculatorSalaryController@luong_cong_viec');
    Route::post('/luong_cong_viec_tc', 'CalculatorSalaryController@luong_cong_viec_tc');
    Route::post('get-level-for-user','GetTypeLevel@get_level_user_type')->name('get_level_user_type');

});
