<?php

Auth::routes();

//Route::get('/test', 'TestController@index')->name('test');
Route::get('/privacy-policy', 'HomeController@privacy_policy')->name('privacy_policy');
Route::get('/terms-of-use', 'HomeController@terms_of_use')->name('terms_of_use');
Route::post('/covid-api','HomeController@covid_api')->name('covid_api');

Route::get('/', 'HomeController@ladingpage')->name('home_index');


Route::group(['prefix' => 'cms'], function() {


    Route::post('loginpost-face','FaceController@loginFace')->name('loginpostface');
    Route::get('login-face','FaceController@login')->name('loginface');


    Route::group(['middleware' => 'auth'], function () {

        Route::get('face-recogn-index','FaceController@index')->name('face.index');
        Route::post('face-recogn-post','FaceController@post')->name('face.post');
        Route::post('face-recogn-detect','FaceController@detect')->name('face.detect');
        Route::get('face-recogn-create-person','FaceController@create_person')->name('face.createper');
        Route::post('face-recogn-create-person-post','FaceController@create_person_post')->name('face.postper');


        Route::get('/type_for_user/staff', 'TypeForUser@staff')->name('typeuser.staff');
        Route::post('/type_for_user/staff', 'TypeForUser@save_staff')->name('typeuser.save_staff');
        Route::get('/type_for_user/staff/{c_id}', 'TypeForUser@get_update_staff')->name('typeuser.get_update_staff');
        Route::put('/type_for_user/staff_update/{u_id}', 'TypeForUser@update_staff')->name('typeuser.update_staff');
        Route::get('/type_for_user/teacher', 'TypeForUser@teacher')->name('typeuser.teacher');
        Route::post('/type_for_user/teacher', 'TypeForUser@save_teacher')->name('typeuser.save_teacher');
        Route::get('/type_for_user/teacher/{c_id}', 'TypeForUser@get_update_teacher')->name('typeuser.get_update_teacher');
        Route::put('/type_for_user/teacher/{u_id}', 'TypeForUser@update_teacher')->name('typeuser.update_teacher');
        Route::delete('/type_for_user/staff/destroy/{d_s_id}', 'TypeForUser@destroy_staff')->name('typeuser.destroy_staff');
        Route::delete('/type_for_user/teacher/destroy/{d_t_id}', 'TypeForUser@destroy_teacher')->name('typeuser.destroy_teacher');

        Route::get('/', 'HomeController@dashboard')->name('home');
        Route::get('/home', 'HomeController@dashboard')->name('home');
        Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

        Route::group(['prefix' => 'my_account'], function() {
            Route::get('/', 'MyAccountController@edit_profile')->name('my_account');
            Route::put('/', 'MyAccountController@update_profile')->name('my_account.update');
            Route::put('/change_password', 'MyAccountController@change_pass')->name('my_account.change_pass');
        });

        /*************** Support Team *****************/
        Route::group(['namespace' => 'SupportTeam',], function(){

            /*************** Dergee *****************/
            Route::get('/dergee', 'DergeeTeacherController@index')->name('dergee.index');
            Route::post('/dergee', 'DergeeTeacherController@save')->name('dergee.save');
            Route::get('/dergee/{c_id}', 'DergeeTeacherController@get_update')->name('dergee.get_update');
            Route::put('/dergee/{u_id}', 'DergeeTeacherController@update')->name('dergee.update');
            Route::delete('/dergee/destroy/{d_t_id}', 'DergeeTeacherController@destroy')->name('dergee.destroy');

            /*************** Students *****************/
            Route::group(['prefix' => 'students'], function(){
                Route::get('reset_pass/{st_id}', 'StudentRecordController@reset_pass')->name('st.reset_pass');
                Route::get('graduated', 'StudentRecordController@graduated')->name('students.graduated');
                Route::put('not_graduated/{id}', 'StudentRecordController@not_graduated')->name('st.not_graduated');
                Route::get('list/{class_id}', 'StudentRecordController@listByClass')->name('students.list');

                /* Promotions */
                Route::post('promote_selector', 'PromotionController@selector')->name('students.promote_selector');
                Route::get('promotion/manage', 'PromotionController@manage')->name('students.promotion_manage');
                Route::delete('promotion/reset/{pid}', 'PromotionController@reset')->name('students.promotion_reset');
                Route::delete('promotion/reset_all', 'PromotionController@reset_all')->name('students.promotion_reset_all');
                Route::get('promotion/{fc?}/{fs?}/{tc?}/{ts?}', 'PromotionController@promotion')->name('students.promotion');
                Route::post('promote/{fc}/{fs}/{tc}/{ts}', 'PromotionController@promote')->name('students.promote');

            });

            /*************** Users *****************/
            Route::group(['prefix' => 'users'], function(){
                Route::get('reset_pass/{id}', 'UserController@reset_pass')->name('users.reset_pass');
            });

            /*************** TimeTables *****************/
            Route::group(['prefix' => 'timetables'], function(){
                Route::get('/', 'TimeTableController@index')->name('tt.index');

                Route::group(['middleware' => 'teamSA'], function() {
                    Route::post('/', 'TimeTableController@store')->name('tt.store');
                    Route::put('/{tt}', 'TimeTableController@update')->name('tt.update');
                    Route::delete('/{tt}', 'TimeTableController@delete')->name('tt.delete');
                });

                /*************** TimeTable Records *****************/
                Route::group(['prefix' => 'records'], function(){

                    Route::group(['middleware' => 'teamSA'], function(){
                        Route::get('manage/{ttr}', 'TimeTableController@manage')->name('ttr.manage');
                        Route::post('/', 'TimeTableController@store_record')->name('ttr.store');
                        Route::get('edit/{ttr}', 'TimeTableController@edit_record')->name('ttr.edit');
                        Route::put('/{ttr}', 'TimeTableController@update_record')->name('ttr.update');
                    });

                    Route::get('show/{ttr}', 'TimeTableController@show_record')->name('ttr.show');
                    Route::get('print/{ttr}', 'TimeTableController@print_record')->name('ttr.print');
                    Route::delete('/{ttr}', 'TimeTableController@delete_record')->name('ttr.destroy');

                });

                /*************** Time Slots *****************/
                Route::group(['prefix' => 'time_slots', 'middleware' => 'teamSA'], function(){
                    Route::post('/', 'TimeTableController@store_time_slot')->name('ts.store');
                    Route::post('/use/{ttr}', 'TimeTableController@use_time_slot')->name('ts.use');
                    Route::get('edit/{ts}', 'TimeTableController@edit_time_slot')->name('ts.edit');
                    Route::delete('/{ts}', 'TimeTableController@delete_time_slot')->name('ts.destroy');
                    Route::put('/{ts}', 'TimeTableController@update_time_slot')->name('ts.update');
                });

            });

            /*************** Payments *****************/
            Route::group(['prefix' => 'payments'], function(){

                Route::get('manage/{class_id?}', 'PaymentController@manage')->name('payments.manage');
                Route::get('invoice/{id}/{year?}', 'PaymentController@invoice')->name('payments.invoice');
                Route::get('receipts/{id}', 'PaymentController@receipts')->name('payments.receipts');
                Route::get('pdf_receipts/{id}', 'PaymentController@pdf_receipts')->name('payments.pdf_receipts');
                Route::post('select_year', 'PaymentController@select_year')->name('payments.select_year');
                Route::post('select_class', 'PaymentController@select_class')->name('payments.select_class');
                Route::delete('reset_record/{id}', 'PaymentController@reset_record')->name('payments.reset_record');
                Route::post('pay_now/{id}', 'PaymentController@pay_now')->name('payments.pay_now');
            });

            /*************** Pins *****************/
            Route::group(['prefix' => 'pins'], function(){
                Route::get('create', 'PinController@create')->name('pins.create');
                Route::get('/', 'PinController@index')->name('pins.index');
                Route::post('/', 'PinController@store')->name('pins.store');
                Route::get('enter/{id}', 'PinController@enter_pin')->name('pins.enter');
                Route::post('verify/{id}', 'PinController@verify')->name('pins.verify');
                Route::delete('/', 'PinController@destroy')->name('pins.destroy');
            });

            /*************** Marks *****************/
            Route::group(['prefix' => 'marks'], function(){

            // FOR teamSA
                Route::group(['middleware' => 'teamSA'], function(){
                    Route::get('batch_fix', 'MarkController@batch_fix')->name('marks.batch_fix');
                    Route::put('batch_update', 'MarkController@batch_update')->name('marks.batch_update');
                    Route::get('tabulation/{exam?}/{class?}/{sec_id?}', 'MarkController@tabulation')->name('marks.tabulation');
                    Route::post('tabulation', 'MarkController@tabulation_select')->name('marks.tabulation_select');
                    Route::get('tabulation/print/{exam}/{class}/{sec_id}', 'MarkController@print_tabulation')->name('marks.print_tabulation');
                });

                // FOR teamSAT
                Route::group(['middleware' => 'teamSAT'], function(){
                    Route::get('/', 'MarkController@index')->name('marks.index');
                    Route::get('manage/{exam}/{class}/{section}/{subject}', 'MarkController@manage')->name('marks.manage');
                    Route::put('update/{exam}/{class}/{section}/{subject}', 'MarkController@update')->name('marks.update');
                    Route::put('comment_update/{exr_id}', 'MarkController@comment_update')->name('marks.comment_update');
                    Route::put('skills_update/{skill}/{exr_id}', 'MarkController@skills_update')->name('marks.skills_update');
                    Route::post('selector', 'MarkController@selector')->name('marks.selector');
                    Route::get('bulk/{class?}/{section?}', 'MarkController@bulk')->name('marks.bulk');
                    Route::post('bulk', 'MarkController@bulk_select')->name('marks.bulk_select');
                });

                Route::get('select_year/{id}', 'MarkController@year_selector')->name('marks.year_selector');
                Route::post('select_year/{id}', 'MarkController@year_selected')->name('marks.year_select');
                Route::get('show/{id}/{year}', 'MarkController@show')->name('marks.show');
                Route::get('print/{id}/{exam_id}/{year}', 'MarkController@print_view')->name('marks.print');

            });

            Route::resource('students', 'StudentRecordController');
            Route::resource('users', 'UserController');
            Route::resource('classes', 'MyClassController');
            Route::resource('sections', 'SectionController');
            Route::resource('subjects', 'SubjectController');
            Route::resource('grades', 'GradeController');
            Route::resource('exams', 'ExamController');
            Route::resource('dorms', 'DormController');
            Route::resource('payments', 'PaymentController');

        });

        /************************ AJAX ****************************/
        Route::group(['prefix' => 'ajax'], function() {
            Route::get('get_lga/{state_id}', 'AjaxController@get_lga')->name('get_lga');
            Route::get('get_class_sections/{class_id}', 'AjaxController@get_class_sections')->name('get_class_sections');
            Route::get('get_class_subjects/{class_id}', 'AjaxController@get_class_subjects')->name('get_class_subjects');
        });

    });

    /************************ SUPER ADMIN ****************************/
    Route::group(['namespace' => 'SuperAdmin','middleware' => 'super_admin', 'prefix' => 'super_admin'], function(){

        Route::get('/settings', 'SettingController@index')->name('settings');
        Route::put('/settings', 'SettingController@update')->name('settings.update');

    });

    /************************ PARENT ****************************/
    Route::group(['namespace' => 'MyParent'], function(){

        Route::get('/my_children', 'MyController@children')->name('my_children');

    });

    /************************ SALARY ****************************/
    Route::group(['namespace' => 'salary'], function(){

        Route::get('/salary-calculate', 'SalaryCalculatorController@index')->name('salary.salary_calculate');
        Route::get('/salary-calculate-ajax', 'SalaryCalculatorController@ajax')->name('salary.salary_calculate_ajax');
        Route::post('/get-position-user', 'SalaryCalculatorController@getPositionUser')->name('salary.get_position_user');
        Route::post('/get-level-type', 'SalaryCalculatorController@getLevelType')->name('salary.get_level_type');
        Route::post('/calculator-salary', 'SalaryCalculatorController@Calculator')->name('salary.calculator_salary');
        Route::post('/save-noti', 'SalaryCalculatorController@SaveNoty')->name('salary.SaveNoty');
        Route::post('/save-without-noti', 'SalaryCalculatorController@SavewithoutSaveNoty')->name('salary.withoutSaveNoty');
        // accountant
        Route::get('/accountant-salary', 'SalaryCalculatorController@Accountantindex')->name('salary.accountant_salary');
        Route::post('/accountant-salary-calculate', 'SalaryCalculatorController@AccountantCalculator')->name('salary.accountant_salary_calculate');
        Route::post('/accountant-get-position-user', 'SalaryCalculatorController@AccountantgetPositionUser')->name('salary.accountant_get_position_user');
        Route::post('/accountant-get-level-type', 'SalaryCalculatorController@accountantGetLevelType')->name('salary.accountant_get_level_type');
        Route::post('/accountant-save-noti', 'SalaryCalculatorController@AccountantSaveNoty')->name('salary.AccountantSaveNoty');
        Route::post('/accountant-save-without-noti', 'SalaryCalculatorController@AccountantSavewithoutSaveNoty')->name('salary.AccountantwithoutSaveNoty');

        /************************ SALARY REPORT ****************************/
        Route::get('/salary-report', 'SalaryReportController@index')->name('salary.report');
        Route::post('/salary-report-list-user', 'SalaryReportController@ListUser')->name('salary.users');
        Route::post('/salary-report-ajax', 'SalaryReportController@ajax')->name('salary.report_ajax');

        /************************ SALARY PARAMETER ****************************/
        Route::get('/STAFF-parameter', 'SalaryParameterController@index')->name('para.staff');
        Route::post('/STAFF-parameter-post', 'SalaryParameterController@create')->name('para.create');
        Route::get('/STAFF-parameter-update/{salary_id}', 'SalaryParameterController@getupdate')->name('para.update');
        Route::put('/STAFF-parameter-update-ajax/{salary_id}', 'SalaryParameterController@update')->name('para.update_ajax');
        Route::delete('/STAFF-parameter-delete', 'SalaryParameterController@create')->name('para.delete');

    });

    
});
